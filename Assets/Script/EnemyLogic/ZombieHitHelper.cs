﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieHitHelper : MonoBehaviour
{
    public ZombieAI parent;
    public HardZombieAi parent2;
    public GameObject bloods;
    
    public void CanWalk()
    {
        parent.walk = true;
    }
    public void ChangeSpeed(float value)
    {
        parent.walk_speed = value;
    }
    void OnTriggerEnter2D(Collider2D coll)
    {

        if (coll.gameObject.tag == "Bullet")
        {

            if (parent != null && parent.health > 0)
            {
                // parent.NearestHitPoint(coll.transform.position);
                if (Mathf.Abs(coll.gameObject.GetComponent<Bullet>().force.x) > 0)
                {
                    parent.Hit(coll.gameObject.GetComponent<Bullet>().damage, (coll.gameObject.GetComponent<Bullet>().force.x > 0) ? 1 : -1);
                    parent.hitpoint = parent.NearestHitPoint(coll.transform.position);
                    var b = Instantiate(bloods);

                    if (parent.health > 0)
                    {
                        b.transform.position = GetComponent<Collider2D>().bounds.ClosestPoint(coll.gameObject.transform.position);
                        b.transform.SetParent(transform);
                    }
                    else b.transform.position = parent.hitpoint.transform.position;
                    // b.transform.SetParent(transform);
                    // b.transform.position = new Vector3(coll.gameObject.transform.position.x, coll.gameObject.transform.position.y, 0);
                }
                
                    
                
                Destroy(coll.gameObject);
            }
            if (parent2 != null && parent2.health > 0)
            {
                if (Mathf.Abs(coll.gameObject.GetComponent<Bullet>().force.x) > 0)
                {
                    parent2.NearestHitPoint(coll.transform.position);
                    parent2.Hit(coll.gameObject.GetComponent<Bullet>().damage, (coll.gameObject.GetComponent<Bullet>().force.x > 0) ? 1 : -1);
                    var b = Instantiate(bloods);
                    b.transform.position = coll.gameObject.transform.position;
                }
                Destroy(coll.gameObject);

            }
            //  coll.gameObject.GetComponent<Bullet>().damage /= 2;
            // 

        }
     

    }
}
