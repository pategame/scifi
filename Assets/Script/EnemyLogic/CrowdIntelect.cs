﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrowdIntelect : MonoBehaviour {

    public List<ZombieAI> crowd;
    public GameObject Player;
    int direction;
    bool walk,attack;

    void Start()
    {
        direction = 0;
       // StartCoroutine(CrowdLogic());
    }
    public void PlayerGone()
    {
        CrowdStay();
        StartCoroutine(CrowdLogic());
        Player = null;
    }
    void CrowdStay()
    {
        if (crowd.Count>0)
        foreach(ZombieAI zombi in crowd)
        {
            zombi.direction = 0;
            zombi.walk = false;
            zombi.Stay();

        }
    }
    void SomebodyInCrowdWalk(ZombieAI zombi)
    {
        zombi.direction = direction;
        zombi.walk = true;
        zombi.gameObject.transform.localScale = new Vector3(-direction, 1, 1);
        zombi.Walk();
    }
    void SomebodyInCrowdStay(ZombieAI zombi)
    {
        zombi.direction = 0;
        zombi.walk = false;
        zombi.Stay();
    }
    
    void CrowdWalk()
    {
        if (crowd.Count>0)
        foreach (ZombieAI zombi in crowd)
        {
            zombi.direction = direction;
           // zombi.walk = true;
           // zombi.walk_speed = 1;
            zombi.gameObject.transform.localScale = new Vector3(-direction, 1, 1); 
            zombi.Walk();

        }
    }
    void CrowdRun()
    {
        foreach (ZombieAI zombi in crowd)
        {
            zombi.direction = direction;
           // zombi.walk = true;
           // zombi.walk_speed = 1;
            zombi.gameObject.transform.localScale = new Vector3(-direction, 1, 1);
            // zombi.Walk();
            zombi.StopAllCoroutines();
            StartCoroutine(Randomizer2(zombi));

        }
        
    }
    IEnumerator Randomizer(ZombieAI zombi)
    {
        yield return new WaitForSecondsRealtime(Random.Range(0.1f, 0.15f));
        // zombi.walk_speed = 3;
        zombi.walk = true;
        zombi.Run();
    }
    IEnumerator Randomizer2(ZombieAI zombi)
    {
        yield return new WaitForSecondsRealtime(Random.Range(0.5f, 1f));
        // print("hohohohoo");
        
        
        zombi.Walk();
        zombi.Attached.PlayInFixedTime("Walk_" + zombi.Attached.GetInteger("Type"), 0,0);    
        //zombi.walk = true;
        StartCoroutine(Randomizer(zombi));
    }
    public void FindPlayer(int _direction)
    {
        if (crowd.Count > 0)
        {
            StopAllCoroutines();
            direction = _direction;
            CrowdRun();

         //   Debug.Log("З'їжте його хлопці");
        }
    }
    IEnumerator CrowdLogic()
    {
        while (Player == null && crowd.Count> 0)
        {
            int rnd = Random.Range(0, 100);
            if (rnd < 25)
            {
                direction = 0;
               // CrowdStay();
            }
            if (rnd > 25)
            {
                rnd = Random.Range(0, 100);
                rnd = rnd > 50 ? 1 : -1;
                direction = rnd;
                rnd = Random.Range(0, crowd.Count);
                SomebodyInCrowdWalk(crowd[rnd]);
                yield return new WaitForSeconds(2);
                SomebodyInCrowdStay(crowd[rnd]);
                // CrowdWalk();
                // Walk();



            }
            yield return new WaitForSeconds(Random.Range(3,7));
        }
      
    }
 
	
}
