﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ZombieAI : MonoBehaviour {
    public Animator Attached;
    public int direction = 0;
    public Material Shadow, defaultmat;
    public float health,walk_speed;
    public bool Civil,inCrowd,nearplayer;
    public bool die,walk,agr;
    public CrowdIntelect crowdInfo;
    Rigidbody2D[] parts;
    [HideInInspector]
    public GameObject hitpoint;
    List<SpriteRenderer> spriteparts;
    RaycastHit2D hit;
    LayerMask layermask = ~((1 << 9) | (1 << 11) | (1 << 13) | (1 << 12) | (1 << 10) | (1 << 16) | (1 << 14));
    LayerMask layerPlayer = ((1 << 0) | (1 << 9) | (1 << 11) | (1 << 13));
    public BoxCollider2D[] hands;

	
	void Start ()
    {
        spriteparts = new List<SpriteRenderer>();
        getAllSprites();
        Attached = GetComponentInChildren<Animator>();
        if (Civil == true)
        {
            Attached.SetInteger("Type", Random.Range(1, 6));
            walk_speed = 1;
        }
        hit = new RaycastHit2D();
        health = 100f;
        die = false;
     
        direction = 1;
        transform.localScale = new Vector3(-direction, 1, 1);
        parts = transform.GetChild(0).GetComponentsInChildren<Rigidbody2D>();
        if (Civil)
        {
            StartCoroutine(RandomizePattern());
            StartCoroutine(SomeLogic());
            
        }
        foreach (BoxCollider2D hand in hands)
        {
            hand.gameObject.tag = "Zombie/hands";
            hand.gameObject.AddComponent<Rigidbody2D>().isKinematic = true;
        }
        //StartCoroutine(WalkAround());

    }
    void getAllSprites()
    {
        SpriteRenderer[] spritesInRagdoll = transform.GetChild(0).GetComponentsInChildren<SpriteRenderer>();
        SpriteRenderer[] spritesInNorma = transform.GetChild(1).GetComponentsInChildren<SpriteRenderer>();
        for (int i = 0; i < spritesInRagdoll.Length; i++)
        {
            spriteparts.Add(spritesInRagdoll[i]);
        }
        for (int i = 0; i < spritesInNorma.Length; i++)
        {
            spriteparts.Add(spritesInNorma[i]);
        }
    }
    IEnumerator RandomizePattern()
    {
        Attached.enabled = false;
        yield return new WaitForSeconds(Random.Range(0.2f, 0.5f));
        Attached.enabled = true;
    }
    void Update()
    {
        if (walk == true && (GetComponent<Renderer>().isVisible == true || inCrowd || agr))
        {
            transform.position += new Vector3(walk_speed * direction * Time.deltaTime, 0, 0);
          
        }
        if (Civil && ((inCrowd && crowdInfo.Player == null)|| !agr))
        {
            CheckPlayer();
        }
        //super kostil
        if (Civil && (Attached.GetBool("Run") == true && !walk))
        {
            walk = true;
        }
    }
    public  void Walk()
    {
        walk_speed = 1;
        if (!Civil)
        {
            if (Attached.GetBool("walk") == false)
                Attached.SetBool("walk", true);
        }
        else
        {
            if (Attached != null && Attached.GetBool("Walk") == false)
                Attached.SetBool("Walk", true);
        }
     
        
        
    }
    void CheckPlayer()
    {
        hit = Physics2D.Raycast(new Vector3(transform.position.x, transform.position.y + 2, 0), new Vector3(-transform.localScale.x, 0, 0), 7f, ~layerPlayer);
        if (hit != false && hit.collider.gameObject.tag == "Player")
        {
            PlayerFound(hit.collider.gameObject);
            //crowdInfo.Player = hit.collider.gameObject;
           // crowdInfo.FindPlayer(hit.collider.gameObject.transform.position.x < transform.position.x ? -1: 1);
        }
        hit = Physics2D.Raycast(new Vector3(transform.position.x, transform.position.y + 2, 0), new Vector3(transform.localScale.x, 0, 0), 2.5f, ~layerPlayer);
        if (hit != false && hit.collider.gameObject.tag == "Player")
        {
            PlayerFound(hit.collider.gameObject);
            //crowdInfo.Player = hit.collider.gameObject;
            // crowdInfo.FindPlayer(hit.collider.gameObject.transform.position.x < transform.position.x ? -1: 1);
        }
    }
    void check()
    {
        hit = Physics2D.Raycast(new Vector3(transform.position.x,transform.position.y+1,0), new Vector3(-transform.localScale.x,0,0), 1f,layermask);
        if (hit != false)
        {
            if (hit.collider.gameObject.tag != "Room" && hit.collider.gameObject.tag != "Elevator" && hit.collider.gameObject.tag != "EnterStation")
            {
                direction = direction * (-1);
                transform.localScale = new Vector3(-direction, 1, 1);
            }

        }
       
    }
    public void Run()
    {
        
        Attached.SetBool("Run",true);
        walk_speed = 4f;
    }
    public void Stay()
    {

        if (!Civil)
            Attached.SetBool("walk", false);
        else
        {
            Attached.SetBool("Run", false);
            Attached.SetBool("Walk", false);
        }
    }
    

    IEnumerator SomeLogic()
    {
        while (die == false)
        {
            yield return new WaitForSeconds(Random.Range(2, 5f));
            int rnd = Random.Range(0, 100);
            if (rnd > 0 && rnd < 25)
            {
                direction = 0;
                walk = false;
                Stay();
            }
            if (rnd > 25 && rnd < 50)
            {
                rnd = Random.Range(0, 100);
                rnd = rnd > 50 ? 1 : -1;
                direction = rnd;
                transform.localScale = new Vector3(-direction, 1, 1);
                walk_speed = 1;
               // walk = true;
                Walk();
                yield return new WaitForSeconds(1);
                walk = false;
                Stay();
            }
            if (rnd > 50)
            {
                walk = false;
                Stay();
                Attached.PlayInFixedTime("Raw_" + Attached.GetInteger("Type").ToString(), 0, 0);
            }
            
        }
    }
	IEnumerator WalkAround()
    {
       
        while (die == false)
        {
            if (direction != 0)
            {
                if (walk == false)
                walk = true;
            }
            else
            {
                walk = false;
                Stay();
            }
            yield return new WaitForSeconds(Time.deltaTime);
           
  
        }
    }
    public void PlusSortLevel(int value)
    {
        var MainBody = transform.GetChild(1).GetComponentsInChildren<SpriteRenderer>();
        var RagDolBody = transform.GetChild(0).GetComponentsInChildren<SpriteRenderer>();

        foreach (SpriteRenderer huh in MainBody)
        {
            huh.sortingOrder += value;
        }
        foreach (SpriteRenderer heh in RagDolBody)
        {
            heh.sortingOrder += value;
        }
    }
    void OnCollisionEnter2D(Collision2D collis)
    {
        if (collis.gameObject.tag == "Car")
        {
            if (collis.gameObject.GetComponentInChildren<CarScript>().speed > 0)
            {
                health = 0;
                NearestHitPoint(collis.contacts[0].point);
                die = true;
                transform.GetChild(1).gameObject.SetActive(false);
           //     int _direction = collis.gameObject.transform.position.x < transform.position.x ? 1 : -1;
                Vector2 force = collis.gameObject.GetComponent<Rigidbody2D>().velocity;
                force.x *= 20;
                force.y = 0;
                collis.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(Mathf.Lerp(collis.gameObject.GetComponent<Rigidbody2D>().velocity.x, 0, 0.5f), collis.gameObject.GetComponent<Rigidbody2D>().velocity.y);
                GameObject body = transform.GetChild(0).gameObject;
                transform.GetChild(0).transform.SetParent(null);
                if (transform.localScale.x == 1)
                    ChangeAngles(body);
                body.SetActive(true);
                if (hitpoint != null)
                    hitpoint.GetComponent<Rigidbody2D>().AddForce(force, ForceMode2D.Impulse);
                Destroy(this.gameObject);
            }

         

        }
    }
    void ChangeAngles(GameObject wherefind)
    {
        var hinges = wherefind.GetComponentsInChildren<HingeJoint2D>();
        foreach (HingeJoint2D hing in hinges)
        {
            //print("SUKA DELAU!");
            JointAngleLimits2D newangles = hing.limits;
            newangles.max *= -1;// hing.limits.min;
            newangles.min *= -1; // hing.limits.max;
            hing.limits = newangles;
        }
    }
    public void PlayerFound(GameObject _Player)
    {
       // print("huli!");
        if (inCrowd && crowdInfo.Player == null)
        {
            crowdInfo.Player = _Player;
            crowdInfo.FindPlayer(_Player.gameObject.transform.position.x < transform.position.x ? -1 : 1);
        }
        if (!inCrowd && !agr)
        {
            direction = _Player.gameObject.transform.position.x < transform.position.x ? -1 : 1;
            Walk();
            Invoke("Run", 0.25f);
            agr = true;
        } 
    }
    public GameObject NearestHitPoint(Vector2 hit)
    {
       
        SpriteRenderer heh = null;
        float distance = 9999;
        for (int i = 0; i < parts.Length; i++)
        {
            heh = parts[i].transform.GetChild(0).GetComponent<SpriteRenderer>();
            Vector2 part = heh.bounds.center;

            if (Vector2.Distance(hit,part) <= distance)
            {
                distance = Vector2.Distance(hit, part);
                hitpoint = parts[i].gameObject;
                //print(hitpoint.name);
            }
        }
        return hitpoint;
    }
   
    public void Hit(float value,int _direction)
    {

        if (inCrowd && crowdInfo.Player == null)
        {
            crowdInfo.Player = GameObject.Find("Player");
            crowdInfo.FindPlayer(-_direction);
        }
        if (!inCrowd && !nearplayer)
        {
            // print("HUHUHUHUHUHUHUHUHUHUUH");
            agr = true;
            direction = -_direction;
            transform.localScale = new Vector3(-direction, 1, 1);
            StopAllCoroutines();
            if (walk == false)
            {
                Attached.PlayInFixedTime("Walk_" + Attached.GetInteger("Type"), 0);
                walk = true;
                Attached.SetBool("Walk", true);
                Run();
            }
            else
            {
                Run();
            }
        }
            if (health > 0)
            {
                health -= value;
                //  Destroy(coll.gameObject, 0.05f);
            }
            if (health <= 0 && die == false)
            {
            if (inCrowd)
            {
                crowdInfo.crowd.Remove(this);
                if (crowdInfo.crowd.Count == 0) Destroy(crowdInfo.gameObject);
            }
                die = true;
            transform.GetChild(1).gameObject.SetActive(false);
           
            GameObject body = transform.GetChild(0).gameObject;
            transform.GetChild(0).transform.SetParent(null);
            if (transform.localScale.x == 1)
                    ChangeAngles(body);
                body.SetActive(true);
            if (hitpoint != null)
            hitpoint.GetComponent<Rigidbody2D>().AddForce(new Vector2(Random.Range(25, 50) * _direction, 0), ForceMode2D.Impulse);
            body.AddComponent<SpriteRenderer>().sprite = Resources.Load("heh") as Sprite;
            body.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0);
            body.AddComponent<ClearMember>();
            //  body.GetComponent<Rigidbody2D>().centerOfMass = new Vector2(body.GetComponent<Rigidbody2D>().centerOfMass.x, body.GetComponent<Rigidbody2D>().centerOfMass.y + Random.Range(-0.3f, 0.3f));
            //  body.GetComponent<Rigidbody2D>().AddForce(new Vector2(Random.Range(25,50) * _direction, 0), ForceMode2D.Impulse);

            //body.get
            Destroy(this.gameObject);
            }
            
        
    }
    IEnumerator AttackAgain()
    {
        yield return new WaitForSeconds(Random.Range(0.5f, 0.8f));

        Attached.PlayInFixedTime("Walk_" + Attached.GetInteger("Type"), 0, 0);
        walk = true;
        Attached.SetBool("Walk", true);
        Attached.SetBool("Run", true);
    }
    void OnTriggerEnter2D(Collider2D coll)
    {
       
            if (!inCrowd && (coll.gameObject.layer == LayerMask.NameToLayer("Stuff") && coll.tag != "EnterStation"))
            {
                if (Attached.GetBool("Run") == true) Attached.SetBool("Run", false);
                {
                direction = -direction;
                transform.localScale = new Vector3(-direction, 1, 1);
                }
            }

        if (inCrowd && (coll.gameObject.layer == LayerMask.NameToLayer("Stuff") && coll.tag != "EnterStation") && crowdInfo.Player != null)
        {
            crowdInfo.PlayerGone();
        }
        if (coll.tag == "Player")
            {
            if (coll.transform.position.x < transform.position.x) transform.localScale = new Vector3(1, 1, 1);
            else transform.localScale = new Vector3(-1, 1, 1);
            

            if (!Civil)
            {
                Attached.SetBool("walk", false);
                Attached.SetBool("attack", true);
            }
            else
            {
                StopAllCoroutines();
                if (!inCrowd)
                {
                    nearplayer = true;
                    
                }
                walk = false;
                Attached.SetBool("Run", false);
                Attached.SetBool("Walk", false);
                Attached.SetBool("Attack", true);
            }
            
        }
    }
    public void ChangeMaterial(bool shadow)
    {
        if (shadow)
        {
            foreach (SpriteRenderer scr in spriteparts)
            {
                scr.sharedMaterial = Shadow;
            }
        }
        else
        {


            foreach (SpriteRenderer scr in spriteparts)
            {
                scr.sharedMaterial = defaultmat;
            }
        }
    }
    void OnTriggerExit2D(Collider2D coll)
    {
        if (coll.tag == "EnterStation" && coll.gameObject.name == "Enter")
        {
            if (transform.position.x > coll.gameObject.transform.position.x)
            {
                // Follower.instance.ChangeSize(40);
               // Follower.instance.ChangeStats(0.05f, 0, 2.5f);
                ChangeMaterial(true);

            }
            else
            {
                //   Follower.instance.ChangeSize(60);
               // Follower.instance.ChangeStats(0.05f, 0, 2.5f);
                ChangeMaterial(false);

            }
        }
        if (coll.tag == "EnterStation" && coll.gameObject.name == "InRoom")
        {
            // print("HUI!");
            if (transform.position.x > coll.gameObject.transform.position.x)
            {
                // Follower.instance.ChangeSize(40);
                transform.position = new Vector3(transform.position.x, transform.position.y, -0.08f);

            }
            else
            {
                //   Follower.instance.ChangeSize(60);
                transform.position = new Vector3(transform.position.x, transform.position.y, 0.02f);

            }
        }
        if (coll.tag == "Player")
        {
           // Debug.Log("Аташол!");
            if (!Civil)
            {
                Attached.SetBool("attack", false);
                StartCoroutine(SomeLogic());
            }
            else
            {
                
                Attached.SetBool("Attack", false);
                if (inCrowd)
                {
                    StartCoroutine(AttackAgain());
                }
                else
                {
                    nearplayer = false;
                    agr = false;
                    StartCoroutine(SomeLogic());
                }
            }

            
           
            
        }
       
    }
    void OnBecameVisible()
    {
      // Attached.enabled = true;
    }
}
