﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySound : MonoBehaviour {
    public AudioClip[] attack;
    public AudioSource voice;

    public void PlayAttackSound()
    {
        voice.clip = attack[Random.Range(0, attack.Length)];
        voice.Play();
    }

	
}
