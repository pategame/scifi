﻿using UnityEngine;
using System.Collections;

public class HardZombieAi : MonoBehaviour
{
    Animator Attached;
    int direction = 0;
    public float health,walk_speed;
    bool die,walk;
    Rigidbody2D mainbody;
    PolygonCollider2D[] parts;
    GameObject hitpoint;
    public CircleCollider2D[] hands;
    GameObject player;

    // Use this for initialization
    void Start()
    {
        Attached = GetComponentInChildren<Animator>();
        Attached.SetInteger("idle", Random.Range(1, 4));
        health = 100f;
        die = false;
        mainbody = GetComponent<Rigidbody2D>();
        direction = 1;

        parts = transform.GetChild(0).GetComponentsInChildren<PolygonCollider2D>();
        StartCoroutine(SomeLogic());
      //  StartCoroutine(WalkAround());

    }
    void Update()
    {
        if (walk)
        {
            transform.position += new Vector3(walk_speed * direction * Time.deltaTime, 0, 0);
        }
    }
    void Walk()
    {
        Attached.SetBool("walk", true);
        if (player == null)
        {
            walk_speed = 2;
            
            //transform.position += new Vector3(2f * direction * Time.deltaTime, 0, 0);
        }
        else
        {
            walk_speed = 5.5f;
            //transform.position += new Vector3(5.5f * direction * Time.deltaTime, 0, 0);
        }
        transform.localScale = new Vector3(direction * -1, 1, 1);
        
    }
    void check()
    {
        LayerMask layermask = ~((1 << 0) | (1 << 9) | (1 << 11) | (1 << 13) | (1 << 12) | (1 << 16) | (1<<10));
        //        LayerMask layermask = (1 << 17);

        RaycastHit2D hit = Physics2D.Raycast(new Vector3(transform.position.x, transform.position.y + 1, 0), new Vector3(-transform.localScale.x, 0, 0), 1f, layermask);
        // Debug.DrawRay(new Vector3(transform.position.x, transform.position.y + 1, 0), new Vector3(-transform.localScale.x, 0, 0) * 1f, Color.green);
        if (hit != false)
        {
           // print("ONO TEBYA SOZHRET!");
           if (hit.collider.gameObject.layer != 10)
            direction = direction * (-1);
          

        }
        if (player == null)
        {
            LayerMask layermask2 = (1<<10);
            //        LayerMask layermask = (1 << 17);

            RaycastHit2D hit2 = Physics2D.Raycast(new Vector3(transform.position.x, transform.position.y + 1, 0), new Vector3(-transform.localScale.x, 0, 0), 5, layermask2);
            Debug.DrawRay(new Vector3(transform.position.x, transform.position.y + 1, 0), new Vector3(-transform.localScale.x, 0, 0) * 5f, Color.green);
            if (hit2 != false)
            {
                StopCoroutine(SomeLogic());
               // StartCoroutine(WalkAround());
                player = hit2.collider.gameObject;
                Attached.SetInteger("run", Random.Range(1, 3));
                direction = (transform.position.x > player.transform.position.x) ? -1 : 1;
            }
        }

    }
   
    void Stay()
    {
        //walk = false;
        Attached.SetBool("walk", false);
    }
    void FixedUpdate()
    {
        if (player == null) check();
    }
   IEnumerator SomeLogic()
    {
        while (die == false)
        {
            int rnd = Random.Range(0, 100);
            if (rnd > 0 && rnd < 25)
            {
                direction = 0;
                walk = false;
                Stay();
            }
            if (rnd > 25)
            {
                rnd = Random.Range(0, 100);
                rnd = rnd > 50 ? 1 : -1;
                direction = rnd;
                transform.localScale = new Vector3(-direction, 1, 1);
                walk = true;
                Walk();

            }
            yield return new WaitForSeconds(Random.Range(7, 10));
        }
    }
    IEnumerator WalkAround()
    {

        while (die == false)
        {
            if (direction != 0) Walk();
            else Stay();
            yield return new WaitForSeconds(Time.deltaTime);


        }
    }
    public void NearestHitPoint(Vector2 hit)
    {

        SpriteRenderer heh = null;
        float distance = 9999;
        for (int i = 0; i < parts.Length; i++)
        {
            heh = parts[i].transform.GetChild(0).GetComponent<SpriteRenderer>();
            Vector2 part = heh.bounds.center;

            if (Vector2.Distance(hit, part) <= distance)
            {
                distance = Vector2.Distance(hit, part);
                hitpoint = parts[i].gameObject;
                //print(hitpoint.name);
            }
        }
    }
    public void Hit(float value, int _direction)
    {

        if (health > 0)
        {
            health -= value;
            //  Destroy(coll.gameObject, 0.05f);
        }
        if (health <= 0 && die == false)
        {
            die = true;
            transform.GetChild(1).gameObject.SetActive(false);

            GameObject body = transform.GetChild(0).gameObject;
            transform.GetChild(0).transform.SetParent(null);
            if (transform.localScale.x == 1)
                ChangeAngles(body);
            body.SetActive(true);
            if (hitpoint != null)
            hitpoint.GetComponent<Rigidbody2D>().AddForce(new Vector2(Random.Range(25, 50) * _direction, 0), ForceMode2D.Impulse);
            body.AddComponent<SpriteRenderer>().sprite = Resources.Load("heh") as Sprite;
            body.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0);
            body.AddComponent<ClearMember>();
            Destroy(this.gameObject);
        }


    }
    void OnCollisionEnter2D(Collision2D collis)
    {

        if (collis.gameObject.tag == "Car")
        {
            health = 0;
            NearestHitPoint(collis.contacts[0].point);
            die = true;
            transform.GetChild(1).gameObject.SetActive(false);
          //  int _direction = collis.gameObject.transform.position.x < transform.position.x ? 1 : -1;
            Vector2 force = collis.gameObject.GetComponent<Rigidbody2D>().velocity;
            force.x *= 20;
            force.y = 0;
            collis.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(Mathf.Lerp(collis.gameObject.GetComponent<Rigidbody2D>().velocity.x,0,0.5f), collis.gameObject.GetComponent<Rigidbody2D>().velocity.y);
            GameObject body = transform.GetChild(0).gameObject;
            transform.GetChild(0).transform.SetParent(null);
            if (transform.localScale.x == 1)
                ChangeAngles(body);
            body.SetActive(true);
            if (hitpoint != null)
                hitpoint.GetComponent<Rigidbody2D>().AddForce(force, ForceMode2D.Impulse);

            Destroy(this.gameObject);
        }
        
    }
    void ChangeAngles(GameObject wherefind)
    {
        var hinges = wherefind.GetComponentsInChildren<HingeJoint2D>();
        foreach (HingeJoint2D hing in hinges)
        {
            //print("SUKA DELAU!");
            JointAngleLimits2D newangles = hing.limits;
            newangles.max *= -1;// hing.limits.min;
            newangles.min *= -1; // hing.limits.max;
            hing.limits = newangles;
        }
    }



    void OnTriggerEnter2D(Collider2D coll)
    {

        if (coll.tag == "Player")
        {
            if (coll.transform.position.x < transform.position.x) transform.localScale = new Vector3(1, 1, 1);
            else transform.localScale = new Vector3(-1, 1, 1);
            StopAllCoroutines();
            Attached.SetInteger("run", 0);
           
            Attached.SetBool("walk", false);
            Attached.SetInteger("attack", Random.Range(1, 3));
            Attached.SetBool("Attack", true);
        }
    }
    void OnTriggerExit2D(Collider2D coll)
    {
        if (coll.tag == "Player")
        {
            Attached.SetBool("Attack", false);
            if (player != null)
            {
                Attached.SetBool("walk", true);
                Attached.SetInteger("run", Random.Range(1, 3));
                //StartCoroutine(SomeLogic());
                direction = (transform.position.x > player.transform.position.x) ? -1 : 1;
               // StartCoroutine(WalkAround());
            }
            //Attached.SetBool("walk", false);

        }
    }
}
