﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CorridorGenerator : MonoBehaviour
{
    public int section, direction;
    public float height,roomoffset_x;
    public float IntensivLight;
    public List<Light> lightinroom;
    public GameObject start, defultsect, exitsect;
    public bool needexit,lightOn;
    float startposx;
    // Use this for initialization

    void Start()
    {
        getLight();
        if (lightinroom.Count > 0) IntensivLight = lightinroom[0].intensity;
        HardTurnOff();
    }
    public void TurnOn()
    {
        if (lightinroom.Count > 0 && lightOn == false)
        {
            lightOn = true;
            for (int i = 0; i < lightinroom.Count; i++)
            {
                float startstep = Random.Range(0.1f, 0.15f);
                StartCoroutine(_turnOn(lightinroom[i], startstep));
            }
            StartCoroutine(blink());
        }
    }
    public void HardTurnOff()
    {
        foreach (Light ss in lightinroom)
        {
            ss.intensity = 0;
            if (ss.transform.childCount > 0)
                ss.transform.GetChild(0).GetComponent<Light>().intensity = 0;
        }
    }
    public void TurnOff()
    {
        if (lightinroom.Count > 0 && lightOn == true)
        {
            lightOn = false;
            for (int i = 0; i < lightinroom.Count; i++)
            {
                float startstep = Random.Range(0.05f, 0.1f);
                StartCoroutine(_turnOff(lightinroom[i], startstep));
            }
            StopCoroutine(blink());
        }
    }
    IEnumerator _turnOn(Light bulb, float _startstep)
    {
        float time = _startstep;
        //float velocity = 0;
        yield return new WaitForSeconds(Random.Range(0.5f, 1f));
        while (time >= 0)
        {
            time -= 0.025f;
            bulb.intensity = bulb.intensity == 0 ? IntensivLight : 0;
            if (bulb.transform.childCount > 0)
                bulb.transform.GetChild(0).GetComponent<Light>().intensity = bulb.intensity == 0 ? 0 : IntensivLight;

            yield return new WaitForSeconds(time);
        }
        bulb.intensity = IntensivLight;
        if (bulb.transform.childCount > 0)
            bulb.transform.GetChild(0).GetComponent<Light>().intensity = IntensivLight;

    }
    IEnumerator _turnOff(Light bulb, float _startstep)
    {
        float time = _startstep;
        //float velocity = 0;

        while (time >= 0)
        {
            time -= 0.025f;
            bulb.intensity = bulb.intensity == 0 ? IntensivLight : 0;
            if (bulb.transform.childCount > 0)
                bulb.transform.GetChild(0).GetComponent<Light>().intensity = bulb.intensity == 0 ? 0 : IntensivLight;

            yield return new WaitForSeconds(time);
        }
        bulb.intensity = 0;
        if (bulb.transform.childCount > 0)
            bulb.transform.GetChild(0).GetComponent<Light>().intensity = 0;

    }
    IEnumerator blink()
    {
        while (lightOn == true)
        {
            yield return new WaitForSeconds(Random.Range(2, 5));
            if (lightOn == true)
                StartCoroutine(_turnOn(lightinroom[Random.Range(0, lightinroom.Count)], Random.Range(0.05f, 0.1f)));
        }
    }
    void getLight()
    {
        lightinroom = new List<Light>();
        Light[] lights = GetComponentsInChildren<Light>();
        for (int i = 0; i < lights.Length; i++)
        {
            if (!lights[i].transform.parent.name.Contains("indicator"))
            {
                lightinroom.Add(lights[i]);
            }
        }
    }
    public void Hopa()
    {
        GetComponentInChildren<SpriteRenderer>().sortingOrder = Generator.instance.corridorsort ;
        
        Generator.instance.corridorsort -= 1;
        
        GenerateCorridor();
    }
   
    void GenerateCorridor()
    {
        for (int i = 0; i < section; i++)
        {
                GameObject sect = null;
            if (i != section - 1)
            {
                sect = Instantiate(defultsect);
            }
            else
            {
                if (needexit == true)
                    sect = Instantiate(exitsect);
                else sect = Instantiate(defultsect);
            }
                sect.transform.parent = transform;
                sect.transform.localPosition = Vector3.zero;
                sect.transform.localPosition += new Vector3(roomoffset_x*(i+1), 0, 0);
            if (sect.GetComponent<SpriteRenderer>() != null)
                sect.GetComponent<SpriteRenderer>().sortingOrder = Generator.instance.corridorsort;
            else sect.GetComponentInChildren<SpriteRenderer>().sortingOrder = Generator.instance.corridorsort;
                Generator.instance.corridorsort -= 1;
            
            if (direction == -1 && i == section-1)
            {
                SpriteRenderer[] heh = sect.GetComponentsInChildren<SpriteRenderer>();
                foreach (SpriteRenderer huh in heh)
                {
                    if (huh.sortingLayerName == "Back") huh.sortingOrder = huh.sortingOrder + 1000;
                }
            }
        }
        
    }
    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.tag == "Player")
            TurnOn();
        // transform.position -= new Vector3(0, 0, 10);
    }
    void OnTriggerExit2D(Collider2D coll)
    {
        if (coll.tag == "Player")
            TurnOff();
        // transform.position -= new Vector3(0, 0, 10);
    }
}
