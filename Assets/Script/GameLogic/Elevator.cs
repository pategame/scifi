﻿using UnityEngine;
using System.Collections;

public class Elevator : MonoBehaviour {

    public GameObject lift;
    public Transform Player;
    public bool havelift,needgoing,interactiv;
    public Vector3 posoffloor;
    public Elevator secondelev;
	// Use this for initialization
	
	void Start()
    {
        interactiv = havelift;
        lift.SetActive(havelift);
    }
    void Update()
    {
        if (Player != null && interactiv == true)
        {
            if (Input.GetKeyDown(KeyCode.E) && (needgoing == false && havelift == true))
            {
                if (GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("elevator") && Player.transform.parent != lift.transform)
                {
                    StopPlayer();
                    Player.GetComponent<Player>().MoveTo(lift.transform.position);
                    GetComponent<Animator>().PlayInFixedTime("open", 0, 0);
                    interactiv = false;
                    needgoing = true;
                }
            }
        }
    }
    public void StopPlayer()
    {
        if (Player != null)
        {
            Player.GetComponent<Player>().StopAll();
            if (Player.GetComponent<Player>().canmove == true)
                Player.GetComponent<Player>().canmove = false;
            else Player.GetComponent<Player>().canmove = true;
        }
    }
   public void InOut()
    {
        //print(Player);
        if (Player != null)
        {
            //StopPlayer();
            if (Player.transform.parent != lift.transform)
            {
                Player.GetComponent<Player>().ReSort("MiddleBack");
                 Player.transform.position = new Vector3(Player.transform.position.x, Player.transform.position.y, 0.1f);
                Player.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePositionY;
                Player.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
                Player.tag = "Stairs";
                Player.transform.SetParent(lift.transform);
                Follower.instance.ChangeStats(0, 0, 2.5f);
               // Player.GetComponent<Player>().enabled = false;
                //Player.GetComponent<Player>().ChangeFollower(0, 0, 2.5f);
               // print(Player);
               // return;
            }
            else
            {
                //Player.GetComponent<Player>().enabled = false;
                Player.GetComponent<Player>().ReSort("Main");
               Player.transform.position = new Vector3(Player.transform.position.x, Player.transform.position.y, -0.1f);
                Player.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;
                Player.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
                Follower.instance.ChangeStats(0.05f, 0, 2.5f);
                //  Player.GetComponent<Player>().ChangeFollower(0.25f, 2, 2.5f);
                Player.tag = "Player";
                Player.transform.SetParent(null);
            }
            if (lift.transform.GetChild(0).gameObject.activeSelf == true)
            {
                lift.transform.GetChild(0).gameObject.SetActive(false);
                StartCoroutine(lightTurn(0, lift.transform.GetChild(1).GetComponent<Light>()));
                StartCoroutine(lightTurn(0, lift.transform.GetChild(2).GetComponent<Light>()));
              
            }
            else
            {
                lift.transform.GetChild(0).gameObject.SetActive(true);
                lift.transform.GetChild(1).GetComponent<Light>().enabled = true;
                lift.transform.GetChild(2).GetComponent<Light>().enabled = true;
                lift.transform.GetChild(1).GetComponent<Light>().intensity = 0;
                lift.transform.GetChild(2).GetComponent<Light>().intensity = 0;
                StartCoroutine(lightTurn(2, lift.transform.GetChild(1).GetComponent<Light>()));
                StartCoroutine(lightTurn(2, lift.transform.GetChild(2).GetComponent<Light>()));
            }
        }

    }
    IEnumerator lightTurn(float value, Light bulb)
    {
        while (bulb.intensity != value)
        {
            bulb.intensity = Mathf.Lerp(bulb.intensity, value, 0.01f);
            yield return new WaitForSeconds(Time.deltaTime);
        }
    }
    public void StartLift()
    {
        
        if (Player != null && needgoing == true)
        {
           // Player.GetComponent<Player>().enabled = true;
            if (lift.transform.position.y > posoffloor.y)
            {
                
                StartCoroutine(GoDown());
            }
            else
            {
                
                StartCoroutine(GoUp());
            }
        }
    }
    IEnumerator GoDown()
    {
            print("DOWN");
        Vector3 target = posoffloor;
        target.y -= 0.05f;
        Vector3 velocity = Vector3.zero;
        while (lift.transform.position.y > posoffloor.y)
        {
            lift.transform.position = Vector3.SmoothDamp(lift.transform.position, target,ref velocity, 1f, 8);
            yield return new WaitForFixedUpdate();
        }


        //  print("heh");
        secondelev.StopPlayer();
        secondelev.lift = lift;
        secondelev.havelift = true;
        secondelev.interactiv = true;
        interactiv = false;
        havelift = false;
            secondelev.GetComponent<Animator>().PlayInFixedTime("open", 0, 0);




    }
    IEnumerator GoUp()
    {
        print("UP");
        Vector3 target = posoffloor;
        target.y += 0.05f;
        Vector3 velocity = Vector3.zero;
        while (lift.transform.position.y < posoffloor.y)
        {
            lift.transform.position = Vector3.SmoothDamp(lift.transform.position, target, ref velocity, 1f, 8);
            yield return new WaitForFixedUpdate();
        }
        secondelev.StopPlayer();
        secondelev.havelift = true;
        havelift = false;
        secondelev.interactiv = true;
        interactiv = false;
        secondelev.lift = lift;
        secondelev.GetComponent<Animator>().PlayInFixedTime("open", 0, 0);
    }
}
