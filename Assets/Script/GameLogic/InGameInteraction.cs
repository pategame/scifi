﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InGameInteraction : MonoBehaviour {

    public GameObject fordebug,play,sky;
    public GameObject[] enemys,civilianZombie;
    public List<Vector3> spawnpoints;
	// Use this for initialization
	void Start ()
    {
        spawnpoints = new List<Vector3>();
        StartCoroutine(Spawner());
	}
   

    IEnumerator Spawner()
    {
        while (Time.timeScale == 1)
        {
            yield return new WaitForSeconds(5);
            Spawn();
            
        }
    }
    void Spawn()
    {
        if (spawnpoints.Count > 0)
        {
            
            Vector3 spawnpos = spawnpoints[Random.Range(0, spawnpoints.Count)];
            if (Vector3.Distance(spawnpos, play.transform.position) > 20)
            {
                var e = Instantiate(civilianZombie[Random.Range(0,enemys.Length)]);
                e.transform.position = new Vector3(spawnpos.x - 5f, spawnpos.y-2f, -0.01f);
            }
           // else Spawn();
        }
    }
    IEnumerator KILLTHEM(CrowdIntelect crowd)
    {
        yield return new WaitForSeconds(10);
        crowd.Player = play;
        crowd.FindPlayer(play.gameObject.transform.position.x < transform.position.x ? -1 : 1);
    }
    void CreateCrowd()
    {
        float posX = -40;
        GameObject crowdcontroller = new GameObject();
        crowdcontroller.AddComponent<CrowdIntelect>();
        crowdcontroller.GetComponent<CrowdIntelect>().crowd = new List<ZombieAI>();
        int sorting = 100;
        int count = 50;
        for (int i = 0; i < count; i++)
        {
            GameObject crwdPart = Instantiate(civilianZombie[Random.Range(0, civilianZombie.Length)]);
            crwdPart.transform.position = new Vector3(posX + Random.Range(-4f, 24f), - 1.5f, -0.08f);
            crwdPart.transform.SetParent(crowdcontroller.transform);
            crwdPart.GetComponent<ZombieAI>().PlusSortLevel(sorting);
            sorting += 100;
            crwdPart.GetComponent<ZombieAI>().inCrowd = true;
            crwdPart.GetComponent<ZombieAI>().crowdInfo = crowdcontroller.GetComponent<CrowdIntelect>();
            

            crowdcontroller.GetComponent<CrowdIntelect>().crowd.Add(crwdPart.GetComponent<ZombieAI>());
        }
        crowdcontroller.name = "CrowdController";
        StartCoroutine(KILLTHEM(crowdcontroller.GetComponent<CrowdIntelect>()));
        //crowdcontroller.GetComponent<CrowdIntelect>().Player = play;
        //crowdcontroller.GetComponent<CrowdIntelect>().FindPlayer();
       // crowdcontroller.transform.SetParent(floor.transform);
    }
    public void OnOffSky(bool onoff)
    {
        sky.SetActive(onoff);
    }
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) Application.Quit();
        if (Input.GetKeyDown(KeyCode.T)) CreateCrowd();
        if (Input.GetKeyDown(KeyCode.Space)) Application.LoadLevel(0);

		
	}
    public void debugzombie()
    {
        var g = Instantiate(fordebug);
        g.transform.position = new Vector3(14, 1.2f, -0.1f);
    }
    public void SlowMoOn()
    {
        Time.timeScale = 0.5f;
        Invoke("SlowMoOff", 1f);
    }
    void SlowMoOff()
    {
        Time.timeScale = 1f;
    }

    
    static InGameInteraction _instance;
    public static InGameInteraction instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<InGameInteraction>();
            }
            return _instance;
        }
    }
}
