﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    public Vector2 force;
    public float damage;
    void OnCollisionEnter2D(Collision2D collis)
    {
        if (collis.gameObject.tag != "Zombie")
        {
            Destroy(gameObject, 0.05f);
        }

    }
	// Use this for initialization
	
}
