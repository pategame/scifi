﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoDestruct : MonoBehaviour {
    public float TimeofDestroy;
	// Use this for initialization
	void Start ()
    {
        Destroy(this.gameObject, TimeofDestroy);	
	}
	
	void OnCollisionEnter2D(Collision2D coll)
    {
        if (tag == "Bullet" && coll.gameObject.tag != "Zombie/Main") 
        {
            GetComponent<TrailRenderer>().enabled = false;
            GetComponent<Rigidbody2D>().gravityScale = 1;
           // GetComponent<Rigidbody2D>().collisionDetectionMode = CollisionDetectionMode2D.Discrete;
        }
    }
}
