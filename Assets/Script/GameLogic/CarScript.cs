﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarScript : MonoBehaviour {

    bool nearPlayer;
    GameObject _Player;
   public WheelJoint2D[] wheels;
    JointMotor2D motor;
    Rigidbody2D mainbody;
   public float speed;

    void Start()
    {
        speed = 0;
        motor = new JointMotor2D();
        motor.maxMotorTorque = 1000;
        wheels = GetComponentsInParent<WheelJoint2D>();
        mainbody = GetComponentInParent<Rigidbody2D>();
    }
	void Update()
    {
        if (nearPlayer && Input.GetKeyDown(KeyCode.E)) InOut(); 

        if (_Player != null && _Player.transform.parent !=null)
        {
            if (Input.GetKey(KeyCode.D))
            Drive(-1);
            if (Input.GetKey(KeyCode.A))
                Drive(1);
            if (Input.GetAxis("Horizontal") == 0 && (int)motor.motorSpeed != 0) 
            {
                Drive(0);
            }
        }
        if (_Player == null)
        {
            if ((int)motor.motorSpeed != 0)
            {
                Drive(0);
            }
        }
        //print(mainbody.velocity);

    }
    void Drive(float direction)
    {
        switch ((int)direction)
        {
            case (0):
                motor.motorSpeed = Mathf.Lerp(motor.motorSpeed, 0, 0.001f);
                break;
            case (-1):
                if (motor.motorSpeed > 0)
                    motor.motorSpeed += direction * 500 * Time.deltaTime;
                if (motor.motorSpeed > -500)
                motor.motorSpeed += direction * 100 * Time.deltaTime;
                break;
            case (1):
                if (motor.motorSpeed < 0)
                    motor.motorSpeed += direction * 500 * Time.deltaTime;
                if (motor.motorSpeed < 200)
                motor.motorSpeed += direction * 100 * Time.deltaTime;
                break;
        }
       // motor.motorSpeed *= -1;
        //motor.motorSpeed += direction * 100 * Time.deltaTime;
        foreach (WheelJoint2D wheel in wheels)
        {
            wheel.motor = motor;
        }
        speed = Mathf.Abs((int)motor.motorSpeed);

    }
    void InOut()
    {
        if (_Player.transform.parent == null)
        {
            _Player.GetComponent<Player>().StopAll();
            _Player.GetComponent<Player>().canmove = false;
            _Player.GetComponent<Rigidbody2D>().isKinematic = true;
            _Player.GetComponent<Player>().ReSort("Stairs");
            _Player.transform.SetParent(this.transform);
           // _Player.SetActive(false);
            Follower.instance.ChangeSize(60);
            Follower.instance.ChangeStats(0, 0, 2.5f);
        }
        else
        {
            _Player.GetComponent<Player>().canmove = true;
            _Player.GetComponent<Player>().ReSort("Main");
            _Player.GetComponent<Rigidbody2D>().isKinematic = false;
            _Player.transform.SetParent(null);
            //_Player.SetActive(true);
            Follower.instance.ChangeSize(50);
            Follower.instance.ChangeStats(0.05f, 0, 2.5f);
        }
    }
    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.tag == "Player")
        {
            nearPlayer = true;
            _Player = coll.gameObject;
        }
    }
    void OnTriggerExit2D(Collider2D coll)
    {
        if (coll.tag == "Player")
        {
            nearPlayer = false;
            _Player = null;
        }
    }
}
