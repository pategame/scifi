﻿using UnityEngine;
using System.Collections;

public class Parralax : MonoBehaviour {

    public GameObject front, back, middle,sky;
    public Transform target;
  
    Vector3 startpos_p, startpos_f, startpos_b,startpos_s;
 
	// Use this for initialization
	void Start ()
    {
        startpos_b = back.transform.position;
        startpos_f = front.transform.position;
        startpos_p = middle.transform.position;
        startpos_s = sky.transform.position;
        
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (target != null)
        {
            front.transform.position = new Vector3(startpos_f.x - target.transform.position.x * 0.8f, front.transform.position.y, front.transform.position.z);
            back.transform.position = new Vector3(startpos_b.x + target.transform.position.x* 0.0001f, back.transform.position.y, back.transform.position.z) ;
            middle.transform.position = new Vector3(startpos_p.x - target.transform.position.x*0.025f, middle.transform.position.y, middle.transform.position.z);
            sky.transform.position = new Vector3(target.transform.position.x, sky.transform.position.y, sky.transform.position.z);

        }
    }
    public void OnOff(bool onoff = true)
    {
        if (onoff)
        {
            target = null;
        }
        else target = GameObject.Find("Main Camera").transform;
    }
    static Parralax _instance;
    public static Parralax instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<Parralax>();
            }
            return _instance;
        }
    }

}
