﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RndCorridorSection : MonoBehaviour {

    public GameObject[] variables;

    void Start()
    {
        int rnd = Random.Range(0, 100);
        if (rnd > 50)
        {
            GameObject newsect = Instantiate(variables[Random.Range(0, variables.Length)]);
            newsect.transform.position = transform.position;
            newsect.transform.SetParent(transform.parent);
            newsect.GetComponentInChildren<SpriteRenderer>().sortingOrder = GetComponentInChildren<SpriteRenderer>().sortingOrder;
            Destroy(this.gameObject);
        }
    }
}
