﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Generator : MonoBehaviour {

    public GameObject start, elevator,corridor,shahta,big_shahta,lighter,map,big_elev,minielevator;
    public GameObject[] balks_for_lift, lobbys, rooms_without_light, rooms_green, rooms_white, rooms_blue, rooms_red, corridors, elevators, stairs,frontdecor,roomdecor,enemys;
    public Sprite[] letters, numbers;
    public  List<GameObject[]> rooms;
    public int corridorsort = 0;
    
    public int maxrooms, maxfloors, maxroomoffloor;
    public string lettersforthefloor;
    int allrooms, allfloors,sortzombie;
	

	void Start ()
    {

#if !UNITY_EDITOR
        
        maxrooms = 11;
        maxfloors = 2;
        maxroomoffloor = 5;
        if (map != null) { Destroy(map, 0); }
        GenerateMap();
#endif


        //  print(allfloors + " всего этажей" + allrooms + " всего комнат");
    }
    public void GenerateMap()
    {
        rooms = new List<GameObject[]>();
        rooms.Add(rooms_without_light);
        rooms.Add(rooms_blue);
        rooms.Add(rooms_green);
        rooms.Add(rooms_white);
        rooms.Add(rooms_red);
        allrooms = 0;
        allfloors = 0;
        sortzombie = 50;
        map = new GameObject();
        map.name = "AllMap";
        Vector2 posofroom = start.GetComponent<SpriteRenderer>().bounds.max;
        CreateFloor(posofroom.x, 0, Random.Range(2,maxroomoffloor),1,Random.Range(0,rooms.Count),Random.Range(0,corridors.Length));
        
    }
    void ResortLayers(GameObject r, int _layersonfloor)
    {
        var srt = r.GetComponentsInChildren<SpriteRenderer>();
     
        if (r.GetComponentInChildren<TextMesh>() != null)
        {
            r.GetComponentInChildren<TextMesh>().gameObject.GetComponent<Sorter>().SortingOrder -= _layersonfloor;
            r.GetComponentInChildren<TextMesh>().gameObject.GetComponent<Sorter>().changelayer();
        }
        foreach (SpriteRenderer sr in srt)
        {
            if (sr.sortingLayerName == "Back")
            {
                sr.sortingOrder -= _layersonfloor; 
            }
        }
       
    }
    void CreateFloor(float startposx,float height,int _maxroom, int direction, int light,int typecorridor)
    {

        List<GameObject> roomsonfloor = new List<GameObject>();
        GameObject floor = new GameObject();
        GameObject[] _rooms = rooms[light];
        int layersonfloor = 0;
        floor.transform.SetParent(map.transform);
        floor.name = "Floor " + allfloors;
        int rnd = 0;
        bool haveelevator = false;
        for (int i = 0; i < _maxroom; i++)
        {
            if (allrooms == 0)
            {
                GameObject l = Instantiate(lobbys[Random.Range(0,lobbys.Length)]);
                float width;// = 0;//(Mathf.Abs(minim_x_of_room(l).x) + Mathf.Abs(maximum_x_of_room(l).x));
                //float halfheight = (Mathf.Abs(minim_y_of_room(l).y) - Mathf.Abs(maximum_y_of_room(l).y)) / 2;
                l.transform.position = new Vector3(26.78f, height, 0.02f);
                allrooms += 1;
                startposx = maximum_x_of_room(l).x;
                l.transform.SetParent(floor.transform);

                GameObject crd = CreateCorridor(height - 0.45f, Random.Range(2, 5), direction, typecorridor, true);
                width = (Mathf.Abs(minim_x_of_room(crd).x) - Mathf.Abs(maximum_x_of_room(crd).x));
                crd.transform.position = new Vector3(startposx, height - 0.45f, 0);
                startposx = maximum_x_of_room(crd).x;
                crd.transform.SetParent(floor.transform);
                roomsonfloor.Add(crd);
            }
            if (allrooms < maxrooms)
            {
                //Создание рандомной комнаты
                rnd = Random.Range(0, _rooms.Length);
                var r = Instantiate(_rooms[rnd]);
                allrooms += 1;
                //Добавление в лист
                roomsonfloor.Add(r);
                //Определение высоты и ширины
                float width = Mathf.Abs(maximum_x_of_room(r).x - minim_x_of_room(r).x);
               // float halfheight = (Mathf.Abs(minim_y_of_room(r).y) + Mathf.Abs(maximum_y_of_room(r).y)) / 2;
                //Различные перемещения в пространстве
                if (direction == 1)
                {
                    if (r.name.Contains("Boiler"))
                        r.transform.position = new Vector3(startposx + (Mathf.Abs(width / 3.4f)), height, 0);
                    else
                    {
                        if (!r.name.Contains("Hangar"))
                            r.transform.position = new Vector3(startposx + (Mathf.Abs(width / 2.8f)), height, 0);
                        else r.transform.position = new Vector3(startposx + (Mathf.Abs(width / 2.4f)), height, 0);
                        if (r.name.Contains("Infame")) r.transform.position = new Vector3(startposx + (Mathf.Abs(width / 3.1f)), height, 0);
                    }


                    startposx = maximum_x_of_room(r).x;
                    if (i != _maxroom - 1 && allrooms < maxrooms)
                    {
                        GameObject crd = CreateCorridor(height - 0.45f, Random.Range(2, 6), direction, typecorridor, true);
                        crd.transform.position = new Vector3(startposx, height - 0.45f, 0);
                        startposx = maximum_x_of_room(crd).x;
                        crd.transform.SetParent(floor.transform);
                        roomsonfloor.Add(crd);
                        rnd = Random.Range(0, 100); //минилифт
                        if (rnd < 25)
                        {
                            crd = CreateCorridor(height - 0.45f, 1, direction, 0, false);
                            crd.transform.position = new Vector3(startposx, height - 0.45f, 0);
                            startposx = maximum_x_of_room(crd).x;
                            crd.transform.SetParent(floor.transform);

                            GameObject miniElev = Instantiate(minielevator);


                       
                            width = (Mathf.Abs(minim_x_of_room(miniElev).x) + Mathf.Abs(maximum_x_of_room(miniElev).x));
                            miniElev.transform.position = new Vector3(startposx + 1.6f, height-0.45f, 0);
                            float heights = Mathf.Abs(maximum_y_of_room(miniElev).y - minim_y_of_room(miniElev).y);
                            //  print(heights);
                            print(heights + " heights");

                            startposx = maximum_x_of_room(miniElev).x;
                           
                            height = height - Mathf.Abs(heights) + 4.5f;
                            miniElev.transform.GetChild(miniElev.transform.childCount - 2).gameObject.SetActive(true);
                            miniElev.transform.SetParent(floor.transform);
                            crd = CreateCorridor(height - 0.45f, 1, direction, 0, true);
                            crd.transform.position = new Vector3(startposx+1.6f, height - 0.45f, 0);
                            startposx = maximum_x_of_room(crd).x;
                            crd.transform.SetParent(floor.transform);
                            roomsonfloor.Add(crd);
                        }
                        
                    }
                }
                else
                {
                    if (r.name.Contains("Boiler"))
                        r.transform.position = new Vector3(startposx - (Mathf.Abs(width / 3.4f)), height, 0);
                    else
                    {
                        if (!r.name.Contains("Hangar"))
                            r.transform.position = new Vector3(startposx - (Mathf.Abs(width / 2.8f)), height, 0);
                        else r.transform.position = new Vector3(startposx - (Mathf.Abs(width / 2.4f)), height, 0);
                        if (r.name.Contains("Infame")) r.transform.position = new Vector3(startposx - (Mathf.Abs(width / 3.1f)), height, 0);
                    }
                    // r.transform.position += new Vector3(Mathf.Abs(width / 15), 0, 0);
                    startposx = minim_x_of_room(r).x;
                    if (i != _maxroom - 1 && allrooms <maxrooms)
                    {
                        GameObject crd = CreateCorridor(height - 0.45f, Random.Range(2, 6), direction, typecorridor, true);
                        crd.transform.position = new Vector3(startposx - ((crd.GetComponent<CorridorGenerator>().section + 1) * crd.GetComponent<CorridorGenerator>().roomoffset_x) + 1.8f, height - 0.45f, 0);
                        startposx = minim_x_of_room(crd).x;
                        crd.transform.SetParent(floor.transform);
                        roomsonfloor.Add(crd);
                        rnd = Random.Range(0, 100); //минилифт
                        if (rnd < 25)
                        {
                            corridorsort += 10;
                            crd = CreateCorridor(height - 0.45f, 1, direction, 0, true);
                           
                            crd.transform.position = new Vector3(startposx - ((crd.GetComponent<CorridorGenerator>().section + 1) * 3.6f) + 1.6f, height - 0.45f, 0);
                            startposx = minim_x_of_room(crd).x;
                            crd.transform.SetParent(floor.transform);

                            GameObject miniElev = Instantiate(minielevator);



                            width = (Mathf.Abs(minim_x_of_room(miniElev).x) + Mathf.Abs(maximum_x_of_room(miniElev).x));
                            miniElev.transform.position = new Vector3(startposx - 1.6f, height - 0.45f, 0);
                            miniElev.transform.GetChild(miniElev.transform.childCount - 1).gameObject.SetActive(true);
                            float heights = Mathf.Abs(maximum_y_of_room(miniElev).y - minim_y_of_room(miniElev).y);
                            //  print(heights);
                            print(heights + " heights");

                            startposx = minim_x_of_room(miniElev).x;

                            height = height - Mathf.Abs(heights) + 4.5f;

                            miniElev.transform.SetParent(floor.transform);
                            crd = CreateCorridor(height - 0.45f, 1, direction, 0, false);
                            crd.transform.position = new Vector3(startposx - ((crd.GetComponent<CorridorGenerator>().section + 1) * crd.GetComponent<CorridorGenerator>().roomoffset_x) + 3.8f, height - 0.45f, 0);
                            startposx = minim_x_of_room(crd).x;
                            crd.transform.SetParent(floor.transform);
                            roomsonfloor.Add(crd);
                        }
                        /*         if (rnd < 25) //Лестницы
                                 {
                                     corridorsort += 10;
                                     crd = CreateCorridor(height - 0.45f, 1, direction, 0, true);
                                     crd.transform.position = new Vector3(startposx - ((crd.GetComponent<CorridorGenerator>().section + 1) * 3.6f) + 1.8f, height - 0.45f, 0);
                                     startposx = minim_x_of_room(crd).x;
                                     crd.transform.SetParent(floor.transform);
                                     int lenghtstairs = Random.Range(2, 5);
                                     GameObject s = CreateStairs(lenghtstairs, -1, Random.Range(0, stairs.Length));
                                     width = (Mathf.Abs(minim_x_of_room(s).x) + Mathf.Abs(maximum_x_of_room(s).x));
                                     s.transform.position = new Vector3(startposx - 1.3f, height - 1.2f, 0);
                                     float heights = Mathf.Abs(maximum_y_of_room(s).y - minim_y_of_room(s).y);
                                     int _rnd = Random.Range(0, 100);
                                     if (_rnd < 50)
                                     {
                                         s.transform.position = new Vector3(minim_x_of_room(s).x + 0.8f, s.transform.position.y + (heights - 4.8f), 0);
                                         s.transform.localScale = new Vector3(1, 1, 1);
                                         s.transform.position += new Vector3(0.8f, 0, 0);
                                         s.transform.GetChild(s.transform.childCount - 1).name = ((int.Parse(s.transform.GetChild(s.transform.childCount - 1).name)) * -1).ToString();
                                     }
                                     startposx = minim_x_of_room(s).x;

                                     if (height >= 0)
                                     {
                                         if (s.transform.localScale.x == -1)
                                             height = height - Mathf.Abs(heights) + 4.8f;
                                         else
                                             height = height + Mathf.Abs(heights) - 4.8f;
                                     }
                                     else
                                     {
                                         if (s.transform.localScale.x == -1)
                                             height = height - Mathf.Abs(heights) + 4.8f;
                                         else
                                             height = height + Mathf.Abs(heights) - 4.8f;
                                     }
                                     s.transform.SetParent(floor.transform);
                                     crd = CreateCorridor(height - 0.45f, 1, direction, 0, false);
                                     crd.transform.position = new Vector3(startposx - ((crd.GetComponent<CorridorGenerator>().section + 1) * crd.GetComponent<CorridorGenerator>().roomoffset_x) + 1.8f, height - 0.45f, 0);
                                     startposx = minim_x_of_room(crd).x;
                                     crd.transform.SetParent(floor.transform);
                                     roomsonfloor.Add(crd);
                                 }*/
                    }

                }

                r.transform.SetParent(floor.transform);
                layersonfloor += 10 * direction;
                ResortLayers(r, layersonfloor);

            }
                if (allfloors < maxfloors && haveelevator == false)
                {
                    rnd = Random.Range(0, 100);
                if (i == _maxroom - 1 || rnd < 25)
                {
                    allfloors += 1;

                    GameObject elev;
                    if (i == _maxroom - 1)
                    {
                        elev = CreateElevator(startposx, direction, floor, light, height, layersonfloor, false);
                        roomsonfloor.Add(elev);
                    }
                    else
                    {
                        elev =  CreateElevator(startposx, direction, floor, light, height, layersonfloor);
                       
                        roomsonfloor.Add(elev);
                    }
                    if (direction == 1) startposx = maximum_x_of_room(elev).x;
                    else startposx = minim_x_of_room(elev).x;



                    haveelevator = true;


                    
                   
                }
                }
               
            
        }
        print(roomsonfloor[roomsonfloor.Count - 1].name);
        if (!roomsonfloor[roomsonfloor.Count - 1].name.Contains("Elevator"))
        {
            if (direction == 1)
            {
                roomsonfloor[roomsonfloor.Count - 1].GetComponent<RoomLight>().Door.SetActive(false);
                roomsonfloor[roomsonfloor.Count - 1].GetComponent<RoomLight>().EndRight.SetActive(true);
                roomsonfloor[roomsonfloor.Count - 1].GetComponent<RoomLight>().EndRight.GetComponent<SpriteRenderer>().sortingLayerName = "Front";
                roomsonfloor[roomsonfloor.Count - 1].GetComponent<RoomLight>().EndRight.GetComponent<SpriteRenderer>().sortingOrder = 1000;
            }
            else
            {
                roomsonfloor[roomsonfloor.Count - 1].GetComponent<RoomLight>().EndLeft.SetActive(true);
                roomsonfloor[roomsonfloor.Count - 1].GetComponent<RoomLight>().EndLeft.GetComponent<SpriteRenderer>().sortingLayerName = "Front";
                roomsonfloor[roomsonfloor.Count - 1].GetComponent<RoomLight>().EndLeft.GetComponent<SpriteRenderer>().sortingOrder = 1000;
            }
        }
        else
        {
            roomsonfloor.RemoveAt(roomsonfloor.Count - 1);
        }
        for (int i = 0; i < roomsonfloor.Count; i++)
        {
            rnd = Random.Range(0, 100);
            if (rnd>50)
            CreateCrowd(roomsonfloor[i], Random.Range(2, 10));
            CreateRoomDecor(roomsonfloor[i]);
        }
       
    }
    GameObject CreateElevator(float startposx,int direction,GameObject floor,int light,float height,int layersonfloor,bool needExit = true)
    {
        GameObject temp;

        int rnd = Random.Range(0, 100);
        if (rnd > 50)
        {
            GameObject e = Instantiate(elevators[light]);
            temp = e;
            e.transform.GetChild(e.transform.childCount - 1).GetChild(0).GetComponent<SpriteRenderer>().sprite = letters[Random.Range(0, letters.Length)];
            e.transform.GetChild(e.transform.childCount - 1).GetChild(1).GetComponent<SpriteRenderer>().sprite = numbers[allfloors];
            float width = Mathf.Abs(maximum_x_of_room(e).x - minim_x_of_room(e).x); ;
          //  float halfheight = (Mathf.Abs(minim_y_of_room(e).y) - Mathf.Abs(maximum_y_of_room(e).y)) / 2;
            //  print(halfheight);
            if (direction == 1)
            {
                e.transform.position = new Vector3(startposx + (Mathf.Abs(width / 2.4f)), height, 0);
                //  e.transform.position -= new Vector3(Mathf.Abs(width / 10), 0, 0);
                e.GetComponent<Elevator>().havelift = true;
                if (!needExit)
                {
                    e.GetComponent<RoomLight>().Door.SetActive(false);
                    e.GetComponent<RoomLight>().EndRight.SetActive(true);
                    e.GetComponent<RoomLight>().EndRight.GetComponent<SpriteRenderer>().sortingOrder = 1000;
                    e.GetComponent<RoomLight>().EndRight.GetComponent<SpriteRenderer>().sortingLayerName = "Front";
                }
                startposx = maximum_x_of_room(e).x;

            }
            else
            {
                e.transform.position = new Vector3(startposx - (Mathf.Abs(width / 2.4f)), height, 0);
                //e.transform.position += new Vector3(Mathf.Abs(width / 10), 0, 0);
                e.GetComponent<Elevator>().havelift = true;
                if (!needExit)
                {
                    e.GetComponent<RoomLight>().EndLeft.SetActive(true);
                    e.GetComponent<RoomLight>().EndLeft.GetComponent<SpriteRenderer>().sortingLayerName = "Front";
                    e.GetComponent<RoomLight>().EndLeft.GetComponent<SpriteRenderer>().sortingOrder = 1000;
                }
                startposx = minim_x_of_room(e).x;

                // e.transform.localScale = new Vector3(-1, 1, 1);
            }
            int nextlight = Random.Range(0, rooms.Count);
            int nexttypecorr = Random.Range(0, corridors.Length);
            var e2 = Instantiate(elevators[nextlight]);
            int distance = 15;
            GameObject newshaht = CreateElevetorShaht(e.transform.position.x, distance + 1, e.transform.position.y - 0.5f);
            e2.transform.position = new Vector3(e.transform.position.x, height - (distance * 4) - 0.5f, 0);
            e.GetComponent<Elevator>().posoffloor = e2.GetComponent<Elevator>().lift.transform.position;
            e2.GetComponent<Elevator>().posoffloor = e.GetComponent<Elevator>().lift.transform.position;
            e.GetComponent<Elevator>().secondelev = e2.GetComponent<Elevator>();
            e2.GetComponent<Elevator>().secondelev = e.GetComponent<Elevator>();
            layersonfloor += 10 * direction;
            ResortLayers(e, layersonfloor);
            e2.transform.GetChild(e2.transform.childCount - 1).GetChild(0).GetComponent<SpriteRenderer>().sprite = letters[Random.Range(0, letters.Length)];
            e2.transform.GetChild(e2.transform.childCount - 1).GetChild(1).GetComponent<SpriteRenderer>().sprite = numbers[allfloors + 1];
           // roomsonfloor.Add(e);
            e.transform.SetParent(floor.transform);
            e2.transform.SetParent(floor.transform);
            int dir = Random.Range(0, 100);
            if (dir > 50)
            {

                GameObject crd = CreateCorridor(height - (distance * 4) - 0.95f, Random.Range(2, 6), 1, nexttypecorr, true);
                crd.transform.position = new Vector3(maximum_x_of_room(e).x, height - (distance * 4) - 0.95f, 0);
                crd.transform.SetParent(newshaht.transform);
                e2.GetComponent<RoomLight>().EndLeft.SetActive(true);
                e2.GetComponent<RoomLight>().EndLeft.GetComponent<SpriteRenderer>().sortingLayerName = "Front";
                e2.GetComponent<RoomLight>().EndLeft.GetComponent<SpriteRenderer>().sortingOrder = 1000;
                CreateFloor(maximum_x_of_room(crd).x, height - (distance * 4) - 0.5f, Random.Range(2, maxroomoffloor), 1, nextlight, nexttypecorr);

            }
            else
            {

                GameObject crd = CreateCorridor(height - (distance * 4) - 0.95f, Random.Range(2, 6), -1, nexttypecorr, true);
                crd.transform.position = new Vector3(minim_x_of_room(e).x - ((crd.GetComponent<CorridorGenerator>().section + 1) * crd.GetComponent<CorridorGenerator>().roomoffset_x) + 1.6f, height - (distance * 4) - 0.95f, 0);
                crd.transform.SetParent(newshaht.transform);
                e2.GetComponent<RoomLight>().Door.SetActive(false);
                e2.GetComponent<RoomLight>().EndRight.SetActive(true);
                e2.GetComponent<RoomLight>().EndRight.GetComponent<SpriteRenderer>().sortingLayerName = "Front";
                e2.GetComponent<RoomLight>().EndRight.GetComponent<SpriteRenderer>().sortingOrder = 1000;
                CreateFloor(minim_x_of_room(crd).x, height - (distance * 4) - 0.5f, Random.Range(2, maxroomoffloor), -1, nextlight, nexttypecorr);
            }
        }
        else
        {
            GameObject e = Instantiate(big_elev);
            temp = e;
            float width = Mathf.Abs(maximum_x_of_room(e).x - minim_x_of_room(e).x); ;
         //   float halfheight = (Mathf.Abs(minim_y_of_room(e).y) - Mathf.Abs(maximum_y_of_room(e).y)) / 2;
            //  print(halfheight);
            if (direction == 1)
            {
                e.transform.position = new Vector3(startposx + (Mathf.Abs(width / 2.4f)), height, 0);
                if (!needExit)
                {
                    e.GetComponent<RoomLight>().Door.SetActive(false);
                    e.GetComponent<RoomLight>().EndRight.SetActive(true);
                    e.GetComponent<RoomLight>().EndRight.GetComponent<SpriteRenderer>().sortingOrder = 1000;
                    e.GetComponent<RoomLight>().EndRight.GetComponent<SpriteRenderer>().sortingLayerName = "Front";
                }

            }
            else
            {
                e.transform.position = new Vector3(startposx - (Mathf.Abs(width / 2.4f)), height, 0);
                if (!needExit)
                {
                    e.GetComponent<RoomLight>().EndLeft.SetActive(true);
                    e.GetComponent<RoomLight>().EndLeft.GetComponent<SpriteRenderer>().sortingLayerName = "Front";
                    e.GetComponent<RoomLight>().EndLeft.GetComponent<SpriteRenderer>().sortingOrder = 1000;
                }

                // e.transform.localScale = new Vector3(-1, 1, 1);
            }
            int nextlight = Random.Range(0, rooms.Count);
            int nexttypecorr = Random.Range(0, corridors.Length);
            var e2 = Instantiate(big_elev);
            int distance = 5;
            GameObject newshaht = CreateElevetorBigShaht(e.transform.position.x, distance + 1, e.transform.position.y - 7.55f);
            e2.transform.position = new Vector3(e.transform.position.x, height - (distance * 10.35f) - (7.85f + 10.35f), 0);
            e2.transform.GetChild(0).gameObject.SetActive(false);
            e.transform.GetChild(0).GetComponent<BigElevScript>().Up = e.transform.GetChild(0).transform.position.y;
            e.transform.GetChild(0).GetComponent<BigElevScript>().Down = e2.transform.GetChild(0).transform.position.y;
            var sprt = e.transform.GetChild(0).GetComponentsInChildren<SpriteRenderer>();
            foreach (SpriteRenderer sp in sprt)
            {
                sp.sortingOrder += 100;
            }
            layersonfloor += 10 * direction;
            ResortLayers(e, layersonfloor);
            // roomsonfloor.Add(e);
            e.transform.SetParent(floor.transform);
            e2.transform.SetParent(floor.transform);
            int dir = Random.Range(0, 100);
            if (dir > 50)
            {

                GameObject crd = CreateCorridor(height - (distance * 10.35f) - (7.85f + 10.35f), Random.Range(2, 6), 1, nexttypecorr, true);
                crd.transform.position = new Vector3(maximum_x_of_room(e).x, height - (distance * 10.35f) - (7.85f+10.35f+0.45f), 0);
                crd.transform.SetParent(newshaht.transform);
                e2.GetComponent<RoomLight>().EndLeft.SetActive(true);
                e2.GetComponent<RoomLight>().EndLeft.GetComponent<SpriteRenderer>().sortingLayerName = "Front";
                e2.GetComponent<RoomLight>().EndLeft.GetComponent<SpriteRenderer>().sortingOrder = 1000;
                CreateFloor(maximum_x_of_room(crd).x, height - (distance * 10.35f) - (7.85f + 10.35f), Random.Range(2, maxroomoffloor), 1, nextlight, nexttypecorr);

            }
            else
            {

                GameObject crd = CreateCorridor(height - (distance * 10.35f) - (7.85f + 10.35f), Random.Range(2, 6), -1, nexttypecorr, true);
                crd.transform.position = new Vector3(minim_x_of_room(e).x - ((crd.GetComponent<CorridorGenerator>().section + 1) * crd.GetComponent<CorridorGenerator>().roomoffset_x) + 1.6f, height - (distance * 10.35f) - (7.85f + 10.35f+0.45f), 0);
                crd.transform.SetParent(newshaht.transform);
                 e2.GetComponent<RoomLight>().Door.SetActive(false);
                 e2.GetComponent<RoomLight>().EndRight.SetActive(true);
                 e2.GetComponent<RoomLight>().EndRight.GetComponent<SpriteRenderer>().sortingLayerName = "Front";
                 e2.GetComponent<RoomLight>().EndRight.GetComponent<SpriteRenderer>().sortingOrder = 1000;
                CreateFloor(minim_x_of_room(crd).x, height - (distance * 10.35f) - (7.85f + 10.35f), Random.Range(2, maxroomoffloor), -1, nextlight, nexttypecorr);
            }
        }
        return temp;
    }
    GameObject CreateStairs(int lenght,int direction,int variable)
    {
        GameObject s = Instantiate(stairs[variable]);
        
        for (int i = 0; i < lenght; i++)
        {
            GameObject s2 = Instantiate(stairs[variable]);
            s2.transform.SetParent(s.transform);
            s2.transform.localPosition = Vector3.zero;
            s2.transform.localPosition += new Vector3(2.86f*(i+1),-1.87f*(i+1),0);
            s2.GetComponentInChildren<SpriteRenderer>().sortingOrder = -(i + 1);
        }
        float width_c = Mathf.Abs(maximum_x_of_room(s).x - minim_x_of_room(s).x);
        float height_c = (lenght+1)*3 ;
        Vector2 centr = new Vector2((minim_x_of_room(s).x + maximum_x_of_room(s).x)/2, ((minim_y_of_room(s).y + maximum_y_of_room(s).y) / 2)- 1 );
        GameObject stairscoll = new GameObject();
        stairscoll.AddComponent<BoxCollider2D>().size = new Vector2(Mathf.Abs(width_c)-1.8f, Mathf.Abs(height_c));
        stairscoll.GetComponent<BoxCollider2D>().isTrigger = true;
       // 
        stairscoll.transform.position = centr;
        stairscoll.transform.SetParent(s.transform);
        stairscoll.tag = "Stairs";
        stairscoll.name = (-direction).ToString();
        stairscoll.layer = LayerMask.NameToLayer("Default");
        s.transform.localScale = new Vector3(direction, 1, 1);
        return s;
        
    }
   
    void CreateRoomDecor(GameObject _room)
    {
        int rnd = Random.Range(0, 100);
        if (rnd > 50)
        {
            float minX = minim_x_of_room(_room).x;
            rnd = Random.Range(1, 3);
            for (int i = 0; i < rnd; i++)
            {
                GameObject decorObj = Instantiate(roomdecor[Random.Range(0, roomdecor.Length)]);
                decorObj.AddComponent<BoxCollider2D>();
                Vector2 boundsObject = decorObj.GetComponent<BoxCollider2D>().size;
                float offsetY = _room.name.Contains("Corridor") ? 2 : 2.3f;
                
                decorObj.transform.position = new Vector3(minX + (boundsObject.x+2), _room.transform.position.y-offsetY , 0f);
                minX = decorObj.transform.position.x;
                decorObj.GetComponent<SpriteRenderer>().sortingOrder += 1;
                decorObj.GetComponent<BoxCollider2D>().enabled = false;
                decorObj.transform.parent = _room.transform;

            }
        }
        rnd = Random.Range(0, 100);
        if (rnd < 25)
        {
            GameObject frontDecor = Instantiate(frontdecor[Random.Range(0, frontdecor.Length)]);
            frontDecor.transform.position = new Vector3(Random.Range(minim_x_of_room(_room).x, maximum_x_of_room(_room).x), _room.transform.position.y - 1.5f,-2f);
            frontDecor.transform.SetParent(_room.transform);
            frontDecor.GetComponent<SpriteRenderer>().sortingOrder = 1100;
        }
    }
    void CreateEnemys(GameObject room,int count)
    {
        for (int i = 0; i< count; i++)
        {
            GameObject e = Instantiate(enemys[Random.Range(0,enemys.Length)]);
            e.transform.position = new Vector3(((minim_x_of_room(room).x + maximum_x_of_room(room).x) / 2) + Random.Range(-1.5f,1.5f), room.transform.position.y-1.5f,-0.01f);
            e.transform.SetParent(room.transform);
            e.GetComponent<ZombieAI>().PlusSortLevel(sortzombie);
            sortzombie += 50;
        }
    }
    void CreateCrowd(GameObject startRoom,int count)
    {
        GameObject crowdcontroller = new GameObject();
        
        crowdcontroller.AddComponent<CrowdIntelect>();
        crowdcontroller.transform.position = new Vector3((minim_x_of_room(startRoom).x + maximum_x_of_room(startRoom).x) / 2, startRoom.transform.position.y);
        crowdcontroller.GetComponent<CrowdIntelect>().crowd = new List<ZombieAI>();
        for (int i = 0; i < count; i++)
        {
            GameObject crwdPart = Instantiate(enemys[Random.Range(0, enemys.Length)]);
            crwdPart.transform.position = new Vector3(((minim_x_of_room(startRoom).x + maximum_x_of_room(startRoom).x) / 2) + Random.Range(-2f, 2f), startRoom.transform.position.y - 1.5f, -0.01f);
            crwdPart.transform.SetParent(crowdcontroller.transform);
            crwdPart.GetComponent<ZombieAI>().PlusSortLevel(sortzombie);
            crwdPart.GetComponent<ZombieAI>().inCrowd = true;
            crwdPart.GetComponent<ZombieAI>().crowdInfo = crowdcontroller.GetComponent<CrowdIntelect>();
            sortzombie += 50;
            
            crowdcontroller.GetComponent<CrowdIntelect>().crowd.Add(crwdPart.GetComponent<ZombieAI>());
       }
        crowdcontroller.name = "CrowdController";
       // crowdcontroller.AddComponent<BoxCollider2D>();
       // Vector2 size = new Vector2(Mathf.Abs(minim_x_of_room(crowdcontroller).x - maximum_x_of_room(crowdcontroller).x), 4);
       // crowdcontroller.GetComponent<BoxCollider2D>().isTrigger = true;
       // crowdcontroller.GetComponent<BoxCollider2D>().size = size;
        
        crowdcontroller.transform.SetParent(startRoom.transform);
    }
    GameObject CreateCorridor(float height,int section, int direction,int _typecorridor,bool needexit)
    {
        GameObject c = Instantiate(corridors[_typecorridor]);
        c.GetComponent<CorridorGenerator>().height = height;
        c.GetComponent<CorridorGenerator>().section = section;
        c.GetComponent<CorridorGenerator>().direction = direction;
        c.GetComponent<CorridorGenerator>().needexit = needexit;
        c.GetComponent<CorridorGenerator>().Hopa();
        float width_c = Mathf.Abs(maximum_x_of_room(c).x - minim_x_of_room(c).x);
        float height_c = 4;
        Vector2 centr = new Vector2((minim_x_of_room(c).x + maximum_x_of_room(c).x) / 2, ((minim_y_of_room(c).y + maximum_y_of_room(c).y) / 2) - 1);
        GameObject corridorColl = new GameObject();
        corridorColl.transform.position = centr;
        corridorColl.transform.SetParent(c.transform);
        c.AddComponent<BoxCollider2D>().size = new Vector2(Mathf.Abs(width_c) - 4f, Mathf.Abs(height_c));
        c.GetComponent<BoxCollider2D>().isTrigger = true;
        //corridorColl.name = "CorridorCollider";
        c.transform.GetComponent<BoxCollider2D>().offset = corridorColl.transform.localPosition;
        DestroyImmediate(corridorColl);
      //  corridorColl.transform.SetParent(c.transform);
       // corridorColl.tag = "Room";
       // corridorColl.layer = LayerMask.NameToLayer("Default");
        return c;
    } 
    GameObject CreateElevetorShaht(float posx,int dist,float _height)
    {
        GameObject allshaht = new GameObject();
        allshaht.name = "for lvl " + (allfloors - 1) + " to " + allfloors;
        allshaht.transform.position = new Vector3(posx, _height);
        for (int i = 0; i < dist; i++)
        {
            GameObject s = Instantiate(shahta);
            s.transform.position = new Vector3(posx-0.5f, _height - (i * 4f),0);
            s.transform.SetParent(allshaht.transform);
            if (i%2 == 0 && (i != 0 && i != dist - 1))
            {
                GameObject balk = Instantiate(balks_for_lift[0]);
                balk.transform.position = new Vector3(posx-0.5f, _height - (i * 4f), -1);
                balk.transform.SetParent(allshaht.transform);
           
            }
            if (i%5 == 0 && (i != 0 && i!= dist-1))
            {
                GameObject anotherbalk = Instantiate(balks_for_lift[Random.Range(1, balks_for_lift.Length)]);
                anotherbalk.transform.position = new Vector3(posx-0.5f, _height - (i * 4f), -1);
                anotherbalk.transform.SetParent(allshaht.transform);

            }
                
        }
        allshaht.transform.SetParent(map.transform);
        return allshaht;
    }
    GameObject CreateElevetorBigShaht(float posx, int dist, float _height)
    {
        GameObject allshaht = new GameObject();
        allshaht.name = "for lvl " + (allfloors - 1) + " to " + allfloors;
        allshaht.transform.position = new Vector3(posx, _height);
        for (int i = 0; i < dist; i++)
        {
            GameObject s = Instantiate(big_shahta);
            s.transform.position = new Vector3(posx, _height - (i * 10.35f), 0);
            var sprt = s.transform.GetChild(0).GetComponentsInChildren<SpriteRenderer>();
            foreach (SpriteRenderer spr in sprt)
            {
                spr.sortingOrder -= 100 * i;
            }
            s.transform.SetParent(allshaht.transform);
           

        }
        allshaht.transform.SetParent(map.transform);
        return allshaht;
    }
    Vector2 minim_y_of_room(GameObject room)
    {
        Vector2 temp = new Vector2(1000000, 100000);


        var children = room.GetComponentsInChildren<SpriteRenderer>();
        for (int i = 0; i < children.Length; i++)
        {
            if (children[i].bounds.min.y < temp.y) temp = children[i].bounds.min;

        }
       // print(temp + " minimum y");
        return temp;
    }

    Vector2 maximum_y_of_room(GameObject room)
    {
        Vector2 temp = new Vector2(-1000000, -1000000);
        var children = room.GetComponentsInChildren<SpriteRenderer>();
        for (int i = 0; i < children.Length; i++)
        {
            if (temp.y < children[i].bounds.max.y) temp = children[i].bounds.max;

        }
       // print(temp + " maximum y");
        return temp;
    }
    public  Vector2 minim_x_of_room(GameObject room)
    {
        Vector2 temp = new Vector2(100000, 100000);



        var children = room.GetComponentsInChildren<SpriteRenderer>();
        for (int i = 0; i < children.Length; i++)
        {
            if (children[i].bounds.min.x <  temp.x ) temp = children[i].bounds.min;

        }
        //print(temp + " minimum " + room.name);
        return temp;
    }
    
    public Vector2 maximum_x_of_room(GameObject room)
    {
        Vector2 temp = new Vector2(-1000000,-1000000);
        var children = room.GetComponentsInChildren<SpriteRenderer>();
        for (int i = 0; i < children.Length; i++)
        {
            if (children[i].bounds.max.x >temp.x ) temp = children[i].bounds.max;

        }
       // print(temp + " maximum " + room.name);
        return temp;
    }
    static Generator _instance;
    public static Generator instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<Generator>();
            }
            return _instance;
        }
    }
}
