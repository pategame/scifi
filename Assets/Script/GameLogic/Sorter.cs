﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sorter : MonoBehaviour {

    public int SortingOrder;
    public string SortingName;
    public bool needto_change_queue;
    public int renderque;

	// Use this for initialization
	void Start ()
    {
        GetComponent<Renderer>().sortingOrder = SortingOrder;
        GetComponent<Renderer>().sortingLayerName = SortingName;
        if (needto_change_queue)
        {
            GetComponent<Renderer>().sharedMaterial.renderQueue = 2000;
        }
		
	}
	
    [ContextMenu("changelayer")]
   public void changelayer()
    {
        GetComponent<Renderer>().sortingOrder = SortingOrder;
        GetComponent<Renderer>().sortingLayerName = SortingName;
        if (needto_change_queue)
        {
            print(GetComponent<Renderer>().sharedMaterial.shader.renderQueue);
            GetComponent<Renderer>().sharedMaterial.renderQueue =2000;
        }
    }
}
