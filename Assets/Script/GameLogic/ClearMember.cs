﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClearMember : MonoBehaviour {

    void Clear()
    {
        var AllRigidsBody = GetComponentsInChildren<Rigidbody2D>();
        var Colliders = GetComponentsInChildren<Collider2D>();
        var Hinge = GetComponentsInChildren<HingeJoint2D>();

        foreach (HingeJoint2D hing in Hinge)
        {
            Destroy(hing);
        }

        foreach (Collider2D coll in Colliders)
        {
            Destroy(coll);
        }
        foreach (Rigidbody2D rigid in AllRigidsBody)
        {
            Destroy(rigid);
        }
    }
    void OnBecameInvisible()
    {
        Invoke("Clear", 10);
    }
}
