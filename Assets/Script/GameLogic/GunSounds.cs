﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunSounds : MonoBehaviour {

    public AudioSource attached;

    public void ShootSounds()
    {
        attached.Play();
    }
}
