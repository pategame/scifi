﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocationGenerator : MonoBehaviour {

    public GameObject[] backobjects, midleobjects, frontobjects;
    public GameObject backparent, midleparent, frontparent,field;
    float min_X, max_X;
    
    // Use this for initialization
	void Start ()
    {
        min_X = minim_x_of_objects(field).x;
        max_X = maximum_x_of_objects(field).x;
     //   print(min_X + " " + max_X);
        GenerateGround();
	}
    void GenerateGround()
    {
        GenerateBackObjects();
        GenerateMiddleObjects();
        GenerateFrontObjects();
    }
    void GenerateBackObjects()
    {
        
        float _max_X = maximum_x_of_objects(field).x;
        float _offset_X = Random.Range(1, 10);
        float width = 0;
        float ypos = 0;
        float rnd = 0;
        float _distance = Mathf.Abs(min_X - _max_X);
        for (int i = 0; i < 100; i++)
        {
           // print(_distance);
            if (_distance> 1)
            {
                GameObject backobj = Instantiate(backobjects[Random.Range(0, backobjects.Length)]);
                ypos = backobj.transform.position.y;
                width = Mathf.Abs(maximum_x_of_object(backobj).x - minim_x_of_object(backobj).x);
                backobj.transform.position = new Vector3(_max_X - ((width / 2) + _offset_X), 0, 0);
                _max_X -= width + _offset_X;
                _distance -= width + _offset_X;
                backobj.transform.parent = backparent.transform;
                backobj.transform.localPosition = new Vector3(backobj.transform.position.x, ypos, 0);
                rnd = Random.Range(0, 100);
                if (rnd > 25) backobj.GetComponent<SpriteRenderer>().flipX = true;
                _offset_X = Random.Range(0, 10);
            }
          
        }
    }
    void GenerateMiddleObjects()
    {
        float _max_X = maximum_x_of_objects(field).x - 10;
        float _offset_X = 0;
        float width = 0;
        float ypos = 0;
        float rnd = 0;
        float _distance = Mathf.Abs(min_X - _max_X);
        for (int i = 0; i < 100; i++)
        {
            if (_distance > 5)
            {
                _offset_X = Random.Range((width / 2), 20);
                GameObject midleObj = Instantiate(midleobjects[Random.Range(0, midleobjects.Length)]);

                ypos = midleObj.transform.position.y;
                width = Mathf.Abs(maximum_x_of_object(midleObj).x - minim_x_of_object(midleObj).x);
                _offset_X = Random.Range((width / 2), 20);
                midleObj.transform.position = new Vector3(_max_X - ((width / 2) + _offset_X ), 0, 0);
                _max_X -= width + _offset_X;
                _distance -= width + _offset_X;
                midleObj.transform.parent = midleparent.transform;
                midleObj.transform.localPosition = new Vector3(midleObj.transform.position.x, ypos, 0);
                midleObj.GetComponent<SpriteRenderer>().sortingOrder += 1;
                rnd = Random.Range(0, 100);
                if (rnd > 25) midleObj.GetComponent<SpriteRenderer>().flipX = true;
                _offset_X = Random.Range((width/2), 20);
            }
          
        }
    }
    void GenerateFrontObjects()
    {
        float _max_X = maximum_x_of_objects(field).x;
        float _offset_X = Random.Range(1, 30);
        float width = 0;
        float ypos = 0;
        float rnd = 0;
        float _distance = Mathf.Abs(min_X - _max_X);
        for (int i = 0; i < 100; i++)
        {
            if (_distance > 10)
            {
                GameObject frontObj = Instantiate(frontobjects[Random.Range(0, frontobjects.Length)]);
                ypos = frontObj.transform.position.y;
                width = Mathf.Abs(maximum_x_of_object(frontObj).x - minim_x_of_object(frontObj).x);
                frontObj.transform.position = new Vector3(_max_X - ((width / 2) + _offset_X), 0, 0);
                _max_X -= width + _offset_X;
                _distance -= width + _offset_X;
                frontObj.transform.parent = frontparent.transform;
                frontObj.transform.localPosition = new Vector3(frontObj.transform.position.x, ypos, 0);
                rnd = Random.Range(0, 100);
                if (rnd > 50) frontObj.GetComponent<SpriteRenderer>().flipX = true;
                _offset_X = Random.Range(0, 30);
            }
            
        }
    }
    public Vector2 minim_x_of_object(GameObject obj)
    {
        Vector2 temp = new Vector2(100000, 100000);
        var children = obj.GetComponent<SpriteRenderer>();
        temp = children.bounds.min;
        return temp;
    }

    public Vector2 maximum_x_of_object(GameObject obj)
    {
        Vector2 temp = new Vector2(-1000000, -1000000);
        var children = obj.GetComponent<SpriteRenderer>();
        temp = children.bounds.max;
        return temp;
    }

    public Vector2 minim_x_of_objects(GameObject objects)
    {
        Vector2 temp = new Vector2(100000, 100000);



        var children = objects.GetComponentsInChildren<SpriteRenderer>();
        for (int i = 0; i < children.Length; i++)
        {
            if (children[i].bounds.min.x < temp.x) temp = children[i].bounds.min;

        }
        //print(temp + " minimum " + room.name);
        return temp;
    }

    public Vector2 maximum_x_of_objects(GameObject objects)
    {
        Vector2 temp = new Vector2(-1000000, -1000000);
        var children = objects.GetComponentsInChildren<SpriteRenderer>();
        for (int i = 0; i < children.Length; i++)
        {
            if (children[i].bounds.max.x > temp.x) temp = children[i].bounds.max;

        }
        // print(temp + " maximum " + room.name);
        return temp;
    }
}
