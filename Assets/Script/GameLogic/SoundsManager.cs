﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundsManager : MonoBehaviour
{

    AudioSource attached;
    public GameObject sounds;

    void Start()
    {
        attached = GetComponent<AudioSource>();
        //  DontDestroyOnLoad(gameObject);
    }

    public void Play(string name, float volume = 1f)
    {

        attached.clip = Resources.Load<AudioClip>("Sound/" + name);
        GameObject copy = this.gameObject;
        var g = Instantiate(copy) as GameObject;
        if (attached.volume > 0)
            g.GetComponent<AudioSource>().volume = volume;
        g.GetComponent<AudioSource>().Play();
        Destroy(g, attached.clip.length);

        //attached.clip = Resources.Load("Sounds/" + name) as AudioClip;
        //  var g = Instantiate(gameObject);
        //  g.GetComponent<AudioSource>().Play();

    }
    public void ChangeSprite()
    {
        if (attached.volume > 0)
        {
            sounds.SetActive(true);

        }
        else
        {
            sounds.SetActive(false);

        }
    }
    public void ShutSound()
    {
        if (attached.volume > 0)
        {
            sounds.SetActive(false);
            attached.volume = 0;
        }
        else
        {
            sounds.SetActive(true);
            attached.volume = 1;
        }
    }

    static SoundsManager _instance;
    public static SoundsManager instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<SoundsManager>();

            }
            return _instance;
        }
    }
}
