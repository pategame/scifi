﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class RoomLight : MonoBehaviour {

    public GameObject EndRight, EndLeft,Door;
    public float IntensivLight;
    public Light[] lightinroom;
    public AnimationCurve LightCurve,AnotherLightCurve;
    bool lightOn = false;
    


    void Start()
    {
        // lightinroom = GetComponentsInChildren<Light>();
        if (lightinroom.Length > 0) 
        IntensivLight = lightinroom[0].intensity != 0 ? lightinroom[0].intensity : 3;
        HardTurnOff();
    }
    
    public void TurnOn()
    {
        if (lightinroom.Length > 0 && lightOn == false)
        {
            lightOn = true;
            for (int i = 0; i < lightinroom.Length; i++)
            {
                float startstep = Random.Range(0.1f, 0.15f);
                StartCoroutine(_turnOn(lightinroom[i],startstep));
            }
            StartCoroutine(blink());
        }
    }
    public void HardTurnOff()
    {
        foreach (Light ss in lightinroom)
        {
            ss.intensity = 0;
            if (ss.transform.childCount > 0)
                ss.transform.GetChild(0).GetComponent<Light>().intensity = 0;
        }
    }
    public void TurnOff()
    {
        if (lightinroom.Length > 0 && lightOn == true)
        {
            lightOn = false;
            for (int i = 0; i < lightinroom.Length; i++)
            {
                float startstep = Random.Range(0.05f, 0.1f);
                StartCoroutine(_turnOff(lightinroom[i], startstep));
            }
            StopCoroutine(blink());
        }
    }
    IEnumerator _turnOn(Light bulb,float _startstep)
    {
        float time = _startstep;
        //float velocity = 0;
        yield return new WaitForSeconds(Random.Range(0.5f, 1f));
        while (time >= 0)
        {
            time -= 0.025f;
            bulb.intensity = bulb.intensity == 0 ? IntensivLight : 0;
            if (bulb.transform.childCount > 0)
            bulb.transform.GetChild(0).GetComponent<Light>().intensity = bulb.intensity == 0 ? 0 : IntensivLight;
            
            yield return new WaitForSeconds(time);
        }
        bulb.intensity = IntensivLight;
        if (bulb.transform.childCount > 0)
            bulb.transform.GetChild(0).GetComponent<Light>().intensity = IntensivLight;

    }
    IEnumerator _turnOff(Light bulb, float _startstep)
    {
        float time = _startstep;
        //float velocity = 0;

        while (time >= 0)
        {
            time -= 0.025f;
            bulb.intensity = bulb.intensity == 0 ? IntensivLight : 0;
            if (bulb.transform.childCount > 0)
                bulb.transform.GetChild(0).GetComponent<Light>().intensity = bulb.intensity == 0 ? 0 : IntensivLight;

            yield return new WaitForSeconds(time);
        }
        bulb.intensity = 0;
        if (bulb.transform.childCount > 0)
            bulb.transform.GetChild(0).GetComponent<Light>().intensity = 0;

    }
    IEnumerator blink()
    {
        while(lightOn == true)
        {
            yield return new WaitForSeconds(Random.Range(2, 5));
            if (lightOn == true)
            StartCoroutine(_turnOn(lightinroom[Random.Range(0, lightinroom.Length)], Random.Range(0.05f, 0.1f)));
        }
    }
 
  
    
    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.tag == "Player")
            TurnOn();
       // transform.position -= new Vector3(0, 0, 10);
    }
    void OnTriggerExit2D(Collider2D coll)
    {
        if (coll.tag == "Player")
            TurnOff();
        // transform.position -= new Vector3(0, 0, 10);
    }
}
