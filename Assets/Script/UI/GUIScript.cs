﻿using System.Collections;
using System.Collections.Generic;
//using UnityEngine.UI;
using UnityEngine;

public class GUIScript : MonoBehaviour {
   // public Image health;
   // public Text AmmoCount;

    public void ChangeHealth(float value)
    {
       // health.fillAmount -= value/100;
       // health.color = Color.Lerp(health.color, Color.red, value / 100);
    }
    public void ChangeAmmoCount(int count)
    {
       // AmmoCount.text = count.ToString() + "/30";
    }
    static GUIScript _instance;
    public static GUIScript instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<GUIScript>();
            }
            return _instance;
        }
    }

}
