﻿using UnityEngine;
using UnityStandardAssets.ImageEffects;
using System.Collections;

public class Follower : MonoBehaviour {
    public Transform target;
    public float offset_x, offset_y, speed;
    public AnimationCurve abberation;
    public VignetteAndChromaticAberration forthelulz;
    Vector3 followtransform, velocity;
    Camera attached;

    // Use this for initialization
    void Start()
    {
        attached = GetComponent<Camera>();
        velocity = Vector3.zero;
    }

    // Update is called once per frame
    public void OnDamage(int direction)
    {
       // if (forthelulz.chromaticAberration == 0)
        StartCoroutine(ChromaticIn(0,direction));
    }
    IEnumerator ChromaticIn(float time,int direction)
    {
        float _time = time;
        while(_time < 1 )
        {
            //forthelulz.chromaticAberration = abberation.Evaluate(_time) * 200;
            offset_x = abberation.Evaluate(_time) * 1.5f * direction;
           // forthelulz.axialAberration = abberation.Evaluate(_time)*10;
            _time += 0.1f;
            yield return new WaitForSeconds(Time.deltaTime/20);

        }
        offset_x = 0;
    }
  
    void Update ()
    {
        if (target != null)
        {
            followtransform = target.position;
            followtransform.x += offset_x*(-target.transform.localScale.x);
            followtransform.y += offset_y;
            followtransform.z = -10;
            transform.position = Vector3.SmoothDamp(transform.position, followtransform, ref velocity, speed);
        }
	}
    public void ChangeStats(float _speed, float _offsetx, float _offsety)
    {
        speed = _speed;
        offset_x = _offsetx;
        offset_y = _offsety;
    }
    public void ChangeSize(float value)
    {
        StopAllCoroutines();
        StartCoroutine(ChangeSizer(value));
    }
    IEnumerator ChangeSizer(float size)
    {
        while (attached.fieldOfView != size)
        {
            attached.fieldOfView = Mathf.MoveTowards(attached.fieldOfView, size, 0.2f);
            yield return new WaitForSecondsRealtime(0.001f);
        }
    }
    static Follower _instance;
    public static Follower instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<Follower>();

            }
            return _instance;
        }
    }
}
