﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerKickHelp : MonoBehaviour {
    public float force;

   void OnCollisionEnter2D(Collision2D collis)
    {
        if(collis.gameObject != null)
        {
            int direction = 1;
            if (collis.gameObject.transform.position.x < transform.position.x) direction = -1;
            collis.gameObject.GetComponent<Rigidbody2D>().AddForce(transform.TransformDirection(Vector2.right) * force * direction, ForceMode2D.Impulse);
            collis.gameObject.GetComponent<Rigidbody2D>().AddTorque(-force * direction, ForceMode2D.Impulse);
        }
    }
    
}
