﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimHelper : MonoBehaviour {

    
    public Player parent;
    public GameObject particles;
	// Use this for initialization

    public void PickupGun()
    {
        parent.GetGun(true);
    }
    public void TurnGun()
    {
        parent.GetGun(false);
    }
    public void LoadAmmo(int count)
    {
        parent.LoadAmmo(count);
        Invoke("OnOffParticles", 0.02f);
    }
    public void ChangeStepSpeed(float value)
    {
        parent.stepspeed = value;
    }
    public void OnOffParticles()
    {
        if (particles.transform.localPosition.z == 0) particles.transform.localPosition = new Vector3(particles.transform.localPosition.x, particles.transform.localPosition.y, 1000);
        else particles.transform.localPosition = new Vector3(particles.transform.localPosition.x, particles.transform.localPosition.y, 0);
    }
}
