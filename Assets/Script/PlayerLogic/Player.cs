﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Player : MonoBehaviour {
    Animator attached, shothand;
    Transform body;
    Vector3 originaldirection;
    public float walk_speed,stepspeed;
    public Material Shadow, defaultmat;
    public GameObject aimhands, fakebody, fakehead, bullets, weapon;
    public GameObject[] normalbody, parts, flashlight, lasersight;
    public int ammocount;
    List<SpriteRenderer> spriteparts;
    Camera screen;
    public  bool aim,canmove;
    float angle_body, angle_hands, angle_head, directionview, health, runspeed,rotationspeed;
    int directionsstair, mmbrdir,direction,rundirection;
    public AudioSource voice;
    public AudioClip[] voice_clips;
    GameObject templadder;

    void Start()
    {
        stepspeed = 1;
        rotationspeed = 0.05f;
        canmove = true;
        spriteparts = new List<SpriteRenderer>();
        attached = GetComponentInChildren<Animator>();
        shothand = aimhands.GetComponent<Animator>();
        body = GetComponent<Transform>();
        originaldirection = body.localScale;
        screen = GameObject.FindObjectOfType<Camera>();
        walk_speed = 1.5f;
        health = 100;
        directionsstair = 0;
        OnOffRagdoll(false);
        getAllSprites();
        ChangeMaterial(false);
        direction = 0;
        rundirection = 0;
        ammocount = 30;
    }
    void getAllSprites()
    {

        for (int i = 0; i < parts.Length; i++)
        {
            for (int j = 0; j < parts[i].transform.childCount; j++)
            {
                if (parts[i].transform.GetChild(j).GetComponent<SpriteRenderer>() != null)
                    spriteparts.Add(parts[i].transform.GetChild(j).GetComponent<SpriteRenderer>());
            }
        }
        var hm = fakebody.GetComponentsInChildren<Transform>();
        for (int i = 0; i < hm.Length; i++)
        {
            if (hm[i].gameObject.GetComponent<SpriteRenderer>() != null)
                spriteparts.Add(hm[i].gameObject.GetComponent<SpriteRenderer>());
        }

    }

    void OnOffRagdoll(bool onoff)
    {
        PolygonCollider2D[] _parts = GetComponentsInChildren<PolygonCollider2D>();
        Collider2D[] normparts = GetComponents<Collider2D>();
        foreach (Collider2D normpart in normparts)
        {
            normpart.enabled = !onoff;

        }
        foreach (PolygonCollider2D part in _parts)
        {
            part.enabled = onoff;
            if (onoff == false)
            {
                part.GetComponent<Rigidbody2D>().isKinematic = true;
            }
            else part.GetComponent<Rigidbody2D>().isKinematic = false;

        }
        parts = new GameObject[_parts.Length];
        for (int i = 0; i < parts.Length; i++)
        {
            parts[i] = _parts[i].gameObject;
        }
    }

    public void ChangeFollower(float speed, float offsetx, float offsety)
    {
        screen.GetComponent<Follower>().speed = speed;
        screen.GetComponent<Follower>().offset_x = offsetx;
        screen.GetComponent<Follower>().offset_y = offsety;
    }

    public void Reload()
    {
        attached.SetBool("aim", false);
        fakebody.SetActive(true);
        GetGun(true);
        shothand.SetBool("shoting", false);
        shothand.SetBool("aim", false);
        shothand.PlayInFixedTime("idle_hands", 0, 0);
        shothand.enabled = false;
        fakebody.GetComponent<Animator>().enabled = true;
        fakebody.GetComponent<Animator>().PlayInFixedTime("reload_gun", 0, 0);
        //fakebody.GetComponent<Animator>().Play("reload_gun");
        
        
        
        
    }
    public void LoadAmmo(int count)
    {
        
        fakebody.GetComponent<Animator>().enabled = false;
        if (aim == true)
        {
             shothand.enabled = true;
             shothand.SetBool("shoting", false);
            shothand.SetBool("aim", true);
            
             shothand.PlayInFixedTime("idle_hands", 0, 0);
            rotationspeed = 0.01f;
            GetGun(false);
            attached.SetBool("aim", true);
            
            //Invoke("BodyRotation", 0.02f);
           // BodyRotation();
        }
        else
        {
            if (aim == false && fakebody.GetComponent<Animator>().enabled == false)
            {
                foreach (GameObject normalparts in normalbody)
                {
                    normalparts.SetActive(true);
                }
                fakebody.SetActive(false);
            }
        }
        ammocount = count;
        GUIScript.instance.ChangeAmmoCount(ammocount);
    }
    void ChangeAngles(GameObject wherefind)
    {
        var hinges = wherefind.GetComponentsInChildren<HingeJoint2D>();
        foreach (HingeJoint2D hing in hinges)
        {
            //print("SUKA DELAU!");
            JointAngleLimits2D newangles = hing.limits;
            newangles.max *= -1;// hing.limits.min;
            newangles.min *= -1; // hing.limits.max;
            hing.limits = newangles;
        }
    }
    void SoundOfShoot()
    {
        LayerMask layerPlayer = ((1 << 10) | (1<<0));
        RaycastHit2D hitRight = Physics2D.Raycast(new Vector3(transform.position.x, transform.position.y + 1, 0), Vector3.right,7f,~layerPlayer);
        if (hitRight != false && hitRight.collider.gameObject.tag == "Zombie")
        {
           // Debug.Log("Какой то хер справа");
            hitRight.collider.gameObject.GetComponent<ZombieAI>().PlayerFound(gameObject);
        }
        RaycastHit2D hitLeft = Physics2D.Raycast(new Vector3(transform.position.x, transform.position.y + 1, 0), Vector3.left, 7f,~layerPlayer);
        
        if (hitLeft != false && hitLeft.collider.gameObject.tag == "Zombie")
        {
          //  Debug.Log("Какой то хер слева");
            hitLeft.collider.gameObject.GetComponent<ZombieAI>().PlayerFound(gameObject);
        }
        Debug.DrawRay(new Vector3(transform.position.x, transform.position.y + 1, 0), Vector3.right * 7f, Color.red);
        Debug.DrawRay(new Vector3(transform.position.x, transform.position.y + 1, 0), Vector3.left * 7f, Color.red);
    }
    public void Shoot()
    {
            GameObject _b = Instantiate(bullets);
            _b.transform.SetParent(weapon.transform);
            _b.transform.localEulerAngles = Vector3.zero;
            _b.transform.localPosition = Vector3.zero;
            _b.transform.localPosition -= new Vector3(0, 0.05f, 0);
            _b.transform.SetParent(null);
            _b.gameObject.layer = LayerMask.NameToLayer("Projectiles");
            _b.gameObject.tag = "Bullet";
            _b.GetComponent<Rigidbody2D>().AddRelativeForce(new Vector2(-transform.localScale.x * 25, 0), ForceMode2D.Impulse);
            //_b.GetComponent<Rigidbody2D>().velocity = _b.transform.TransformDirection(Vector3.right * 45 * -transform.localScale.x) ;
            _b.GetComponent<Bullet>().force = _b.transform.TransformDirection(Vector3.right * 45 * -transform.localScale.x);
            // print(_b.GetComponent<Rigidbody2D>().velocity + "sasasasas");
            ammocount -= 1;
            GUIScript.instance.ChangeAmmoCount(ammocount);
            SoundOfShoot();        
    }
    void OnOffLassers(bool state)
    {
        foreach (GameObject laser in lasersight)
        {
            laser.SetActive(state);
        }
    }
    void OnOffFlashlight(bool state)
    {
        foreach (GameObject light in flashlight)
        {
            light.SetActive(state);
        }
    }
    void Update()
    {
       // print(GetComponent<Rigidbody2D>().velocity);
        if (health > 0)
        {
            if (canmove == true)
            {
                if (Input.GetKeyDown(KeyCode.V))
                {
                    OnOffLassers(!lasersight[0].activeSelf);
                }
                if (Input.GetKeyDown(KeyCode.F))
                {
                    OnOffFlashlight(!flashlight[0].activeSelf);
                }

                if (Input.GetKeyDown(KeyCode.R) && ammocount < 30)
                {
                    Reload();
                }
                //ходьба

                if (Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.D))
                {
                    direction = Input.GetAxis("Horizontal") >= 0 ? 1 : -1;
                }
                if (Input.GetKey(KeyCode.A) && direction == 0)  direction = -1;
                    if (Input.GetKey(KeyCode.D) && direction == 0)  direction = 1; 

                //Бег
                if (Input.GetKeyDown(KeyCode.LeftShift) && direction != 0)
                {
                   // aim = false;
                    //Follower.instance.ChangeSize(60);
                    runspeed = 0;
                }

                if (Input.GetKeyUp(KeyCode.LeftShift)) StopRun();
                if (Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.D)) runspeed = 0;
                if ((Input.GetKey(KeyCode.A) == false && Input.GetKey(KeyCode.D) == false))
                {
                    StopRun();
                    Stop();
                }

                if (direction != 0)
                {
                    if (!aim)
                    {
                        if (!Input.GetKey(KeyCode.LeftShift)) Walk(direction);
                        else Run(direction); 
                    }
                    else
                    {
                        Walk(direction);
                    }
                }
                                              

                if (Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.D)) direction = 0;

                if (aim == false)
                {
                    if (Input.GetKeyUp(KeyCode.A)) attached.SetFloat("speedofwalk", 0);
                    if (Input.GetKeyUp(KeyCode.D)) attached.SetFloat("speedofwalk", 0);
                                                        
                }
                if (aim == true && directionsstair != 0)
                {
                    if (Input.GetKeyUp(KeyCode.A)) attached.SetFloat("speedofwalk", 0);
                    if (Input.GetKeyUp(KeyCode.D)) attached.SetFloat("speedofwalk", 0);
                    if (attached.GetFloat("speedofwalk") == 0)
                    attached.SetInteger("directionsstairs", (int)directionview * (int.Parse(templadder.name)));
                }
                
                if (Input.GetKeyDown(KeyCode.Q)) Kick();
                
                if (Input.GetMouseButton(1) == true && aim == false)
                {
                        if (fakebody.GetComponent<Animator>().enabled == false)
                        {

                            StopRun();
                            GetGun(true);
                            aim = true;
                            attached.SetBool("aim", aim);
                            shothand.enabled = true;
                            shothand.PlayInFixedTime("idle_hands", 0, 0);
                            shothand.SetBool("aim", true);
                            if (templadder != null && attached.GetFloat("speedofwalk") == 0)
                            {
                                GetGun(true);
                            }
                        }

                }
                if (Input.GetMouseButton(1) == false && aim == true)
                {
                    aim = false;
                    attached.SetBool("aim", aim);
                    shothand.SetBool("aim", false);
                    shothand.SetBool("shoting", false);
                    // directionview = 1;
                    if (fakebody.GetComponent<Animator>().enabled == false)
                    GetGun(false);
                   
                }


                if (aim == true)
                {
                    if (Input.GetMouseButtonDown(0) && ammocount == 0)
                    {
                        SoundsManager.instance.Play("noammo", 0.5f);
                    }
                    // if (fakebody.activeSelf == false) GetGun(true);
                    if (Input.GetMouseButton(0))
                    {
                        if (ammocount > 0 && aim == true)
                            shothand.SetBool("shoting", true);
                        if (ammocount <= 0 && aim == true)
                        {
                            shothand.SetBool("shoting", false);
                        }

                    }
                    if (Input.GetMouseButtonUp(0))
                    {
                      //  shothand.PlayInFixedTime("idle_hands", 0, 0);
                        shothand.SetBool("shoting", false);
                    }
                    body.transform.localScale = new Vector3(originaldirection.x * directionview, originaldirection.y, originaldirection.z);
                }

                BodyRotation();
                }
            
         

        }
    }

    void BodyRotation()
    {
        Vector3 pos_mouse = Vector2.zero;
        Ray ray_mouse = screen.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit = new RaycastHit();
        int layermask = LayerMask.NameToLayer("MouseHelper");
        if (Physics.Raycast(ray_mouse, out hit, 1000, 1 << layermask))
        {
            pos_mouse = hit.point;
        }

        pos_mouse.z = 0;
        directionview = transform.position.x > pos_mouse.x ? -1 : 1;

        if (aim == true)
        {
            angle_body = Vector2.Angle(Vector2.up, pos_mouse - fakebody.transform.position);
            if (fakebody.GetComponent<Animator>().enabled == false)
                angle_hands = Vector2.Angle(Vector2.up, pos_mouse - aimhands.transform.position);

            angle_head = Vector2.Angle(Vector2.up, pos_mouse - fakehead.transform.position);
            if (angle_body < 65) angle_body = 65;
            if (angle_body > 105) angle_body = 105;



            if (angle_hands < 40) angle_hands = 40;
            if (angle_hands > 140) angle_hands = 140;



            if (angle_head < 95) angle_head = 95;
            if (angle_head > 105) angle_head = 105;

            //   print("ugol tela " + angle_body + " ugol ruk " + angle_hands + " ugol golovi " + angle_head);
        }
        

      
        if (Mathf.Abs(transform.position.x - pos_mouse.x) > 0.5f && fakebody.activeSelf == true)
        {
            float _angle_b = fakebody.transform.position.x > pos_mouse.x ? (-angle_body + 90f) * directionview : (angle_body - 90f) * directionview;
            fakebody.transform.localEulerAngles = new Vector3(0, 0, Mathf.LerpAngle(fakebody.transform.localEulerAngles.z,_angle_b, rotationspeed));

            float _angle_h = aimhands.transform.position.x > pos_mouse.x ? (-angle_hands + 90f) * directionview : (angle_hands - 90f) * directionview;
            _angle_h -= fakebody.transform.localEulerAngles.z;
            aimhands.transform.localEulerAngles = new Vector3(0, 0, Mathf.LerpAngle(aimhands.transform.localEulerAngles.z, _angle_h, rotationspeed));

            float _angle_head = fakehead.transform.position.x > pos_mouse.x ? (-angle_head + 90f) * directionview : (angle_head - 90f) * directionview;
            fakehead.transform.localEulerAngles = new Vector3(0, 0, Mathf.LerpAngle(fakehead.transform.localEulerAngles.z, _angle_head, rotationspeed));
        }
        if (aim == true && rotationspeed < 0.1f)
        {
            rotationspeed += 0.001f;
        }
        if (aim == false && rotationspeed > 0.05f)
        {
            rotationspeed -= 0.001f;
        }
    }
    void Kick()
    {
        // print(attached.GetCurrentAnimatorStateInfo(0));
        if (attached.GetCurrentAnimatorStateInfo(0).IsName("idle"))
        {
            LayerMask layermask = (1 << 13);
            Vector3 start = new Vector3(transform.position.x + 0.2f, transform.position.y + 0.1f);
            RaycastHit2D hit = Physics2D.Raycast(start, new Vector3(-transform.localScale.x, 0, 0), 1, layermask);
            Debug.DrawRay(start, new Vector3(-transform.localScale.x, 0, 0), Color.red);
            if (hit != false)
            {
                attached.Play("low_kick");

            }
            else
            {
                attached.Play("kick");
            }
        }
    }

    void Walk(int direction)
    {
        if (attached.GetBool("walk") == false)
        attached.SetBool("walk", true);
        if (aim == false) directionview = direction;
        attached.SetFloat("speedofwalk", directionview * direction);
        
        if (directionsstair == 0)
        {
            if (directionview * direction < 0)
            {
                walk_speed = 1;
                attached.speed = 0.75f;
            }
            else
            {
                walk_speed = 1.5f;
                attached.speed = 1f;
            }
        }
        else
        {
            if (directionview * direction < 0)
            {
                walk_speed = 1.75f;
                attached.speed = 1.5f;
            }
            else
            {
                walk_speed = 1.75f;
                attached.speed = 1.5f;
            }
        }
        if (templadder != null)
        {

            if ((mmbrdir != (int)transform.localScale.x) && aim == false)
            {
                directionsstair *= -1;
                mmbrdir = (int)transform.localScale.x;
            }
            if (mmbrdir != -direction && aim == true)
            {
                directionsstair *= -1;
                mmbrdir = -(int)direction;
            }
            // directionsstair = mmbrdir * direction;

            if (directionsstair == 1)
            {
                
                GetComponent<Rigidbody2D>().gravityScale = 0f;
            }
            else
            {
                GetComponent<Rigidbody2D>().gravityScale = 0f;
            }
            if (aim == false)
                attached.SetInteger("directionsstairs", directionsstair);
            else
            {
                attached.SetInteger("directionsstairs", (int)directionview * direction * directionsstair);
            }
        }
        float zspeed = 0;
        if (directionsstair > 0) zspeed = 1;
        if (directionsstair < 0) zspeed = -1.5f;
        body.transform.position += new Vector3((walk_speed * stepspeed) * direction * Time.deltaTime, zspeed * Time.deltaTime);
        if (aim == false)
            body.transform.localScale = new Vector3(originaldirection.x * direction, originaldirection.y, originaldirection.z);
        // Check(direction);


    }

    void Check(int direction)
    {
        RaycastHit2D[] hits = Physics2D.RaycastAll(transform.position, Vector2.down);
        Debug.DrawRay(transform.position, Vector3.down);
        GameObject mainladder = null;
        foreach (RaycastHit2D hit in hits)
        {
            if (hit.collider.gameObject != null && hit.collider.tag == "Stairs")
            {
                if (mainladder == null)
                {
                    mainladder = hit.transform.gameObject.GetComponentInParent<BoxCollider2D>().gameObject;
                }
                if (transform.position.x < Generator.instance.maximum_x_of_room(mainladder).x && direction == 1)
                {

                }
                if (transform.position.x > Generator.instance.minim_x_of_room(mainladder).x && direction == -1)
                {
                    directionsstair = 1;
                    attached.SetBool("stairs", true);
                    attached.SetInteger("directionsstairs", directionsstair);
                }
            }
            if (hit.collider.tag == "Floor")
            {
                directionsstair = 0;
                attached.SetBool("stairs", false);
                attached.SetInteger("directionsstairs", directionsstair);
            }
        }

    }

    public void GetGun(bool onoff)
    {

        if (onoff == true)
        {
            foreach (GameObject normalparts in normalbody)
            {
                normalparts.SetActive(false);
            }
            fakebody.SetActive(true);
        }
        if (onoff == false)
        {
            
                angle_body = 91;
                angle_hands = 145;
                angle_head = 110;
                StartCoroutine(TurnGun());

           // fakehead.transform.localEulerAngles = new Vector3(0, 0, 15);
         
           // aimhands.transform.localEulerAngles = new Vector3(0, 0, 50);
        }
        
    }
    IEnumerator TurnGun()
    {
        yield return new WaitForSeconds(0.6f);
        {
            if (aim == false && fakebody.GetComponent<Animator>().enabled == false)
            {
                foreach (GameObject normalparts in normalbody)
                {
                    normalparts.SetActive(true);
                }
                fakebody.SetActive(false);
            }
        }
    }
    void Run(int direction)
    {
        if (directionsstair == 0)
        {
            if (runspeed < 4)
            {
                runspeed += 0.1f;
            }
            attached.SetFloat("speedofwalk", Mathf.Abs(runspeed) / 4f);
            if (attached.GetBool("walk") == false)
                attached.SetBool("walk", true);
            if (attached.GetBool("run") == false)
            {
                Follower.instance.ChangeSize(60);
                attached.SetBool("run", true);
            }
            body.transform.position += new Vector3(Mathf.Abs(runspeed) * Time.deltaTime * Input.GetAxis("Horizontal"), 0, 0);
            body.transform.localScale = new Vector3(originaldirection.x * direction, originaldirection.y, originaldirection.z);
        }
    }

    void StopRun()
    {
        runspeed = 0;
        attached.SetBool("run", false);
        Follower.instance.ChangeSize(50);
    }
    public void MoveTo(Vector3 pos)
    {
        StartCoroutine(MovingTo(pos));
    }
    IEnumerator MovingTo(Vector3 pos)
    {
        while (Mathf.Abs(transform.position.x - pos.x) > 0.1f)
        {
            Walk((pos.x > transform.position.x) ? 1 : -1);
            yield return new WaitForSeconds(Time.deltaTime/2);
        }
        StopAll();
    }
    public void Stop()
    {
        if (attached.GetBool("walk") != false)
        {
            attached.SetBool("walk", false);
            Follower.instance.ChangeSize(50);
        }
     //   attached.SetBool("run", false);
    }
    public void StopAll()
    {
        if (attached.GetBool("walk") != false)
        {
            attached.SetBool("walk", false);
            Follower.instance.ChangeSize(50);
        }
        attached.SetBool("run", false);
        attached.SetBool("aim", false);
        attached.SetFloat("speedofwalk", 0);
        direction = 0;
        rundirection = 0;
        //   attached.SetBool("run", false);
    }
    void Hit(float value,int direction)
    {
        if (health > 0)
        {
            health -= value;
            GUIScript.instance.ChangeHealth(value);
           // int rnd = Random.Range(0, voice_clips.Length);
            if (voice.isPlaying == false)
            {
               // voice.clip = voice_clips[rnd];
               // voice.Play();
            }
            Follower.instance.OnDamage(((int)transform.localScale.x));
        }
        if (health <= 0)
        {
            InGameInteraction.instance.SlowMoOn();
            GetComponent<Rigidbody2D>().isKinematic = true;
            GetGun(false);
            //OnOffRagdoll(true);
            attached.enabled = false;
            screen.GetComponent<Follower>().target = null;
            GameObject body = transform.GetChild(0).GetChild(0).gameObject;
            if (transform.localScale.x == 1)
            ChangeAngles(transform.GetChild(0).gameObject);
            transform.GetChild(0).gameObject.SetActive(true);
            transform.GetChild(0).SetParent(null);
            body.GetComponent<Rigidbody2D>().AddForce(new Vector2(25 * direction, 25), ForceMode2D.Impulse);
            Destroy(gameObject, 0);
            // body.GetComponent<Rigidbody2D>().AddTorque(-25  * transform.TransformDirection(Vector3.left).x, ForceMode2D.Impulse);
        }

      //  print(health);
    }

    IEnumerator ChangeSize(float size)
    {
        while (screen.orthographicSize != size)
        {
            screen.orthographicSize = Mathf.MoveTowards(screen.orthographicSize, size, 0.05f);
            yield return new WaitForSecondsRealtime(0.001f);
        }
    }
    public void ChangeMaterial(bool shadow)
    {
        if (shadow)
        {
            foreach (SpriteRenderer scr in spriteparts)
            {
                scr.sharedMaterial = Shadow;
            }
        }
        else
        {
           

            foreach (SpriteRenderer scr in spriteparts)
            {
                scr.sharedMaterial = defaultmat;
            }
        }
    }
    public void ReSort(string Layer)
    {
       foreach(GameObject kek in lasersight)
        {
            kek.GetComponent<LineRenderer>().sortingLayerName = Layer;
        }
        foreach (SpriteRenderer sprt in spriteparts)
        {
            sprt.sortingLayerName = Layer;
        }
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.tag == "Elevator" && !coll.name.Contains("Elevator_2") && !coll.name.Contains("BiGElev")) { coll.gameObject.GetComponent<Elevator>().Player = transform; }
        if (coll.tag == "Zombie/hands") Hit(2f, (coll.gameObject.transform.position.x > transform.position.x) ? -1 : 1);
        if (coll.tag == "EnterStation" && coll.gameObject.name == "Enter")
        {
            if (transform.position.x < coll.gameObject.transform.position.x)
            {
                // StartCoroutine(ChangeSize(4));
            //    Follower.instance.ChangeSize(40);
              //  Follower.instance.ChangeStats(0.05f, 2, 2.5f);
               // ChangeMaterial(true);
               // ChangeFollower(0.05f, 2, 2.5f);
               // Parralax.instance.OnOff(true);
            }
            else
            {
                ChangeMaterial(false);
               // Follower.instance.ChangeSize(60);
              //  Follower.instance.ChangeStats(0.05f, 0, 4f);
                
               // Parralax.instance.OnOff(false);
            }
        }
        if (coll.tag == "Stairs")
        {
            templadder = coll.gameObject;

            if (transform.position.y > coll.GetComponent<Collider2D>().bounds.min.y)
            {
                GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                GetComponent<Rigidbody2D>().gravityScale = 0;
                directionsstair = -1;
                mmbrdir = -direction;
                attached.SetBool("stairs", true);
                attached.SetInteger("directionsstairs", directionsstair);
            }
            if (transform.position.y < coll.GetComponent<Collider2D>().bounds.max.y - 4f)
            {
                // GetComponent<Rigidbody2D>().isKinematic = true;
                GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                GetComponent<Rigidbody2D>().gravityScale = 0;
                directionsstair = 1;
                mmbrdir = -direction;
                attached.SetBool("stairs", true);
                attached.SetInteger("directionsstairs", directionsstair);
            }
        }

    }

    void OnTriggerExit2D(Collider2D coll)
    {
        if (coll.tag == "Elevator" && !coll.name.Contains("Elevator_2") && !coll.name.Contains("BiGElev")) { coll.gameObject.GetComponent<Elevator>().Player = null; coll.gameObject.GetComponent<Elevator>().needgoing = false; coll.gameObject.GetComponent<Elevator>().interactiv = true; }
        if (coll.tag == "Stairs")
        {
            templadder = null;
            GetComponent<Rigidbody2D>().isKinematic = false;
            GetComponent<Rigidbody2D>().gravityScale = 1f;
            directionsstair = 0;
            attached.SetBool("stairs", false);
            attached.SetInteger("directionsstairs", directionsstair);
        }
        if (coll.tag == "EnterStation" && coll.gameObject.name == "Enter")
        {
            if (transform.position.x > coll.gameObject.transform.position.x)
            {
               // Follower.instance.ChangeSize(40);
                Follower.instance.ChangeStats(0.05f, 0, 2.5f);
                ChangeMaterial(true);
               
            }
            else
            {
             //   Follower.instance.ChangeSize(60);
                Follower.instance.ChangeStats(0.05f, 0, 2.5f);
                ChangeMaterial(false);
              
            }
        }
        if (coll.tag == "EnterStation" && coll.gameObject.name == "InRoom")
        {
           // print("HUI!");
            if (transform.position.x > coll.gameObject.transform.position.x)
            {
                // Follower.instance.ChangeSize(40);
                transform.position = new Vector3(transform.position.x, transform.position.y, -0.08f);

            }
            else
            {
                //   Follower.instance.ChangeSize(60);
                transform.position = new Vector3(transform.position.x, transform.position.y, 0.02f);

            }
        }
        if (coll.tag == "EnterStation" && coll.gameObject.name == "EnterDungeon")
        {
            //print("HUI!");
            if (transform.position.x > coll.gameObject.transform.position.x)
            {
                // Follower.instance.ChangeSize(40);
                InGameInteraction.instance.OnOffSky(false);

            }
            else
            {
                //   Follower.instance.ChangeSize(60);
                InGameInteraction.instance.OnOffSky(true);

            }
        }
    }
   
}
