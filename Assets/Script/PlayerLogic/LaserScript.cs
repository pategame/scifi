﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserScript : MonoBehaviour {
    public GameObject point;
    public LineRenderer laser;
    public Material pointmat;
    float distance;
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        LayerMask layermask = ~((1 << 0) | (1 << 16) | (1 << 9) );
        if (point.GetComponent<Renderer>().material != pointmat)
        {
            point.GetComponent<Renderer>().sharedMaterial = pointmat;
        }
        RaycastHit2D hit = Physics2D.Raycast(transform.position, transform.TransformDirection(Vector3.forward), 5,layermask);
       // Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward)*5, Color.green);
        if (hit != false)
        {
            distance = Vector2.Distance(transform.position, hit.point);
            //print(distance);

            laser.SetPosition(1, new Vector3(0, 0, distance));
            point.transform.localPosition = new Vector3(0, 0, distance);

        }
        else
        {
            laser.SetPosition(1, new Vector3(0, 0, 5));
            point.transform.localPosition = new Vector3(0, 0, 0);
        }

    }
}
