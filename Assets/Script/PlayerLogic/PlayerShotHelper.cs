﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShotHelper : MonoBehaviour
{
    public Player parent;
    public void Shot()
    {
        parent.Shoot();
        SoundsManager.instance.Play("weapon_shoot",0.4f);
    }
    public void CheckAmmo()
    {
        if (parent.ammocount == 0) parent.Reload();
    }
}
