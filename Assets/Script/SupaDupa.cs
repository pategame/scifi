﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SupaDupa : MonoBehaviour {

    public GameObject effect;
    public bool lamp;
    void OnJointBreak2D(Joint2D brokenJoint)
    {
        // print("nahui!");
        var g = Instantiate(effect);
        g.transform.position = brokenJoint.transform.position;
        g.transform.SetParent(transform.parent);
        
        Destroy(g, 1f);
        brokenJoint.gameObject.transform.SetParent(null);
        brokenJoint.GetComponent<Rigidbody2D>().mass = 1;
        brokenJoint.GetComponent<Rigidbody2D>().velocity = brokenJoint.GetComponent<Rigidbody2D>().velocity = brokenJoint.GetComponent<Rigidbody2D>().velocity/10;
        if (lamp)
        {
            GetComponentInChildren<Light>().gameObject.SetActive(false);
        }
        else
        {
           
        }
        
    }
}
