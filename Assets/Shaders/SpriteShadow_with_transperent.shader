Shader "Custom/SpriteShadow_Transperent" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex("Color (RGB) Alpha (A)", 2D) = "white" {}
		//_Value("Transparent", Range(0,1)) = 0.5
	}
	SubShader {
		Tags 
		{ 
			"Queue"="Geometry"
			"RenderType"="Transparen"
			
		}
		Blend SrcAlpha OneMinusSrcAlpha
		LOD 200
		ZTest Off
		Cull Off
		CGPROGRAM
		// Lambert lighting model, and enable shadows on all light types
		#pragma surface surf Lambert alpha //addshadow fullforwardshadows 

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;
		fixed4 _Color;
		fixed _Value;

		struct Input
		{
			float2 uv_MainTex;
		};

		void surf (Input IN, inout SurfaceOutput o) {
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex);
			clip(c.a - 0.05);
			//o.Emission = _Color.rgb;
			o.Albedo = _Color;
			o.Alpha = _Color.a;
			
			
		}
		ENDCG
	}
	FallBack "Transparent"
}
