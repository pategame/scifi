﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


[CustomEditor(typeof(Generator))]
public class MapGeneratorEditor : Editor
{
    public Generator myTarget;
    public int _maxfloors, _maxrooms, _maxroomoffloor;
   
    void OnEnable()
    {

        myTarget = (Generator)target;
    }

    public override void OnInspectorGUI()
    {

        DrawDefaultInspector();
        _maxfloors = (myTarget.maxfloors);
        _maxrooms = myTarget.maxrooms;
        _maxroomoffloor = myTarget.maxroomoffloor;
        EditorGUILayout.LabelField("Максимум комнат");
        myTarget.maxrooms = EditorGUILayout.IntField(_maxrooms);
        EditorGUILayout.LabelField("Максимум комнат на этаж");
        myTarget.maxroomoffloor = EditorGUILayout.IntField(_maxroomoffloor);
        EditorGUILayout.LabelField("Максимум этажей");
        myTarget.maxfloors = EditorGUILayout.IntField(_maxfloors);
      
       
      
       
        if (GUILayout.Button("Сгенерировать карту"))
        {
           if (myTarget.map == null)
            myTarget.GenerateMap();
            else
            {
                DestroyImmediate(myTarget.map);
                myTarget.GenerateMap();
            }
          
         
        }
        if (GUILayout.Button("Удалить карту"))
        {
            if (myTarget.map != null)
            {
                DestroyImmediate(myTarget.map);
               // myTarget.GenerateMap();
            }


        }
    }

}
