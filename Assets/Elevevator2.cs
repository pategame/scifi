﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Elevevator2 : MonoBehaviour {
    public float Up, Down;
    public GameObject Elevator;
    public Light[] lights;
    bool havePlayer,going;

    void Start()
    {
        if (transform.parent.GetChild(transform.parent.childCount -1 ).gameObject.activeSelf == false)
        {
           // print("AGA!");
            lights = transform.parent.GetChild(transform.parent.childCount - 2).gameObject.GetComponentsInChildren<Light>();
        }
        else lights = transform.parent.GetChild(transform.parent.childCount - 1).gameObject.GetComponentsInChildren<Light>();
    }
    void Update()
    {
        if (havePlayer && Input.GetKey(KeyCode.E) && !going)
        { Go(); }
    }

    void Go()
    {
        if (!going)
        {
            Elevator.transform.GetChild(1).transform.GetChild(0).gameObject.SetActive(false);
            Elevator.transform.GetChild(2).transform.GetChild(0).gameObject.SetActive(true);
            if (Elevator.transform.localPosition.y > Down)
            {
                StartCoroutine(GoDown());
            }
            else StartCoroutine(GoUp());
            going = true;
        }
       
    }
    IEnumerator GoDown()
    {
        Vector3 target = Elevator.transform.localPosition;
        target.y = Down;
        Vector3 velocity = Vector3.zero;
        while (Elevator.transform.localPosition.y > Down)
        {
            Elevator.transform.localPosition = Vector3.SmoothDamp(Elevator.transform.localPosition, target, ref velocity,0.05f, 2);
            yield return new WaitForFixedUpdate();
        }
        Elevator.transform.GetChild(1).transform.GetChild(0).gameObject.SetActive(true);
        Elevator.transform.GetChild(2).transform.GetChild(0).gameObject.SetActive(false);
        going = false;
    }
    IEnumerator GoUp()
    {
        Vector3 target = Elevator.transform.localPosition;
        target.y =  Up;
        Vector3 velocity = Vector3.zero;
        while (Elevator.transform.localPosition.y < Up)
        {
            Elevator.transform.localPosition = Vector3.SmoothDamp(Elevator.transform.localPosition, target, ref velocity, 0.05f, 2);
            yield return new WaitForFixedUpdate();
        }
        Elevator.transform.GetChild(1).transform.GetChild(0).gameObject.SetActive(true);
        Elevator.transform.GetChild(2).transform.GetChild(0).gameObject.SetActive(false);
        going = false;
    }
    IEnumerator TurnLight_Down()
    {
        int i = lights.Length-1;
        while (lights[0].enabled == false)
        {
            yield return new WaitForSeconds(0.25f);
            lights[i].enabled = true;
            i--;
        }
    }
    IEnumerator TurnOffLight_Down()
    {
        int i = lights.Length-1;
        while (lights[0].enabled == true)
        {
            yield return new WaitForSeconds(0.25f);
            lights[i].enabled = false;
            i--;
        }
    }
    IEnumerator TurnLight_Up()
    {
        int i = 0;
        while (lights[lights.Length-1].enabled == false)
        {
            yield return new WaitForSeconds(0.25f);
            lights[i].enabled = true;
            i ++;
        }
    }
    IEnumerator TurnOffLight_Up()
    {
        int i = 0;
        while (lights[lights.Length - 1].enabled == true)
        {
            yield return new WaitForSeconds(0.25f);
            lights[i].enabled = false;
            i++;
        }
    }
    void OnTriggerEnter2D(Collider2D coll)
    {
       // coll.transform.SetParent(Elevator.transform);
        if (coll.tag == "Player")
        {
            coll.transform.SetParent(Elevator.transform);
            havePlayer = true;
            if (Elevator.transform.localPosition.y > Down)
            StartCoroutine(TurnLight_Up());
            else StartCoroutine(TurnLight_Down());
            Follower.instance.ChangeStats(0, 0, 2.5f);
        }
        if (coll.tag == "Zombie")
        {
            coll.transform.SetParent(Elevator.transform);
        }
    }
    void OnTriggerExit2D(Collider2D coll)
    {
        //coll.transform.SetParent(null);
        if (coll.tag == "Player")
        {
            coll.transform.SetParent(null);
            havePlayer = false;
            if (Elevator.transform.localPosition.y > Down)
                StartCoroutine(TurnOffLight_Up());
            else StartCoroutine(TurnOffLight_Down());
            Follower.instance.ChangeStats(0.05f, 0, 2.5f);
        }
        if (coll.tag == "Zombie")
        {
            coll.transform.SetParent(null);
        }

    }
}
