﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigElevScript : MonoBehaviour
{
    public float Up, Down;
    public GameObject Elevator;
    public GameObject Player;
    bool havePlayer, going, needGoing = false;

    void Update()
    {
        if (havePlayer && Input.GetKey(KeyCode.E) && !going)
        {
            if (GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Idle") && Player.transform.parent != Elevator.transform)
            {
                StopPlayer();
                GetComponent<Animator>().PlayInFixedTime("Open", 0, 0);
                needGoing = true;
            }
            //Go();
        }
    }

    void Go()
    {
        if (!going && Player != null && needGoing)
        {
            Elevator.transform.GetChild(0).gameObject.SetActive(true);
           // Player.GetComponent<Player>().ReSort("MiddleBack");
           // GetComponent<Animator>().Play("Open");
            if (Elevator.transform.position.y > Down)
            {
                StartCoroutine(GoDown());
            }
            else StartCoroutine(GoUp());
            going = true;
        }

    }
    public void StopPlayer()
    {
        if (Player != null)
        {
            Player.GetComponent<Player>().StopAll();
            if (Player.GetComponent<Player>().canmove == true)
                Player.GetComponent<Player>().canmove = false;
            else Player.GetComponent<Player>().canmove = true;
        }
    }
    public void InOut()
    {
        if (Player != null)
        {
            //StopPlayer();
            if (Player.transform.parent != Elevator.transform)
            {
                Player.GetComponent<Player>().ReSort("MiddleBack");
               // Player.transform.position = new Vector3(Player.transform.position.x, Player.transform.position.y, 0.1f);
              //  Player.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePositionY;
               // Player.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
             //   Player.tag = "Stairs";
                Player.transform.SetParent(Elevator.transform);
         //       Follower.instance.ChangeStats(0, 0, 2.5f);
                // Player.GetComponent<Player>().enabled = false;
                //Player.GetComponent<Player>().ChangeFollower(0, 0, 2.5f);
                // print(Player);
                // return;
            }
            else
            {
                //Player.GetComponent<Player>().enabled = false;
                Player.GetComponent<Player>().ReSort("Main");
             //   Player.transform.position = new Vector3(Player.transform.position.x, Player.transform.position.y, -0.1f);
             //   Player.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;
               // Player.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
              //  Follower.instance.ChangeStats(0.05f, 0, 2.5f);
                //  Player.GetComponent<Player>().ChangeFollower(0.25f, 2, 2.5f);
              //  Player.tag = "Player";
                Player.transform.SetParent(null);
            }
        }
    }
    IEnumerator GoDown()
    {
        Vector3 target = Elevator.transform.position;
        target.y = Down;
        Vector3 velocity = Vector3.zero;
        while (Elevator.transform.position.y > Down)
        {
            Elevator.transform.position = Vector3.SmoothDamp(Elevator.transform.position, target, ref velocity, 0.05f, 4);
            yield return new WaitForFixedUpdate();
        }
        StopPlayer();
        Elevator.transform.GetChild(0).gameObject.SetActive(false);
        GetComponent<Animator>().PlayInFixedTime("Open", 0, 0);
        // Player.GetComponent<Player>().ReSort("Main");
        going = false;
        needGoing = false;
    }
    IEnumerator GoUp()
    {
        Vector3 target = Elevator.transform.position;
        target.y = Up;
        Vector3 velocity = Vector3.zero;
        while (Elevator.transform.position.y < Up)
        {
            Elevator.transform.position = Vector3.SmoothDamp(Elevator.transform.position, target, ref velocity, 0.05f, 4);
            yield return new WaitForFixedUpdate();
        }
        StopPlayer();
        Elevator.transform.GetChild(0).gameObject.SetActive(false);
        GetComponent<Animator>().PlayInFixedTime("Open", 0, 0);
        //  Player.GetComponent<Player>().ReSort("Main");
        going = false;
        needGoing = false;
    }
    void OnTriggerEnter2D(Collider2D coll)
    {
        // coll.transform.SetParent(Elevator.transform);
        if (coll.tag == "Player")
        {
            Player = coll.gameObject;
           // coll.transform.SetParent(Elevator.transform);
            havePlayer = true;
            Follower.instance.ChangeStats(0, 0, 2.5f);
        }
    }
    void OnTriggerExit2D(Collider2D coll)
    {
        //coll.transform.SetParent(null);
        if (coll.tag == "Player")
        {
            Player = null;
           // coll.transform.SetParent(null);
            havePlayer = false;
            Follower.instance.ChangeStats(0.05f, 0, 2.5f);
        }

    }
}
