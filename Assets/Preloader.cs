﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Preloader : MonoBehaviour {

	// Use this for initialization
	void Start ()
    {
        Invoke("Load", 0.5f);
		
	}
	
	// Update is called once per frame
    void Load()
    {
        Application.LoadLevel(1);
    }
	
}
